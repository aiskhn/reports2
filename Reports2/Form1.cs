﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using TS = Tekla.Structures;
using TSM = Tekla.Structures.Model;
using TSMO = Tekla.Structures.Model.Operations;

using Newtonsoft.Json;
using System.IO;
using OfficeOpenXml;
using System.Linq;

using MetroFramework.Forms;

namespace Reports2
{
    public partial class MainForm : MetroForm
    {
        TSM.Model _model;
        Document new_document;
        Dictionary<string, Control> listControl;
        Dictionary<string, MyGrids> listGrid;

        private TSM.Model Model => _model ?? (_model = new TSM.Model());

        /// <summary>
        /// Initial method to run app
        /// </summary>
        public MainForm()
        {
            try
            {
                InitializeComponent();

                this.BorderStyle = MetroFormBorderStyle.FixedSingle;
                this.ShadowType = MetroFormShadowType.AeroShadow;

                MyTools.GetConfigurationValue();
                CollectControls();

                MyTools.PutLogo(pictureBox1, tabMain);
                MyTools.GetRevisions(c_rev);
                MyTools.GetEnvironments(c_environment);
                MyTools.GetTemplates(c_environment, c_template);

                LoadReports();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Loading reports files to checkedListBox, they are needed to select
        /// </summary>
        private void LoadReports()
        {
            if (!Model.GetConnectionStatus())
                return;

            Text += $" - for {TS.TeklaStructuresInfo.GetCurrentProgramVersion()}";

            Text += $", ver. {System.Reflection.Assembly.GetExecutingAssembly().GetName().Version}";

            var _reports = new List<string>();
            MyTools.GetReports(TeklaStuff.allAttributes, out _reports);

            var modelPath = _model.GetInfo().ModelPath;

            if (_reports.Count == 0)
                MyTools.ReportsFromPath(modelPath, ref _reports);

            checkedLists.Items.Add("STAL", false);

            for (int i = 0; i < MyGlobal.allowedLists.Length; i++)
            {
                var foundList = _reports.Find(x => x.Contains($"{MyGlobal.TitleReports}{MyGlobal.allowedLists[i]}"));
                if (foundList != null)
                {
                    if (!checkedLists.Items.Contains(foundList))
                        checkedLists.Items.Add(foundList, false);
                }
            }

            checkedLists.CheckOnClick = true;

            MyGlobal.FolderOutputs = Path.Combine(modelPath, "Reports");
            if (!Directory.Exists(MyGlobal.FolderOutputs))
                Directory.CreateDirectory(MyGlobal.FolderOutputs);

            MyGlobal.FolderReports = MyGlobal.FolderOutputs;

            MyGlobal.FolderDocuments = Path.Combine(modelPath, MyGlobal.FolderDocuments);
            if (!Directory.Exists(MyGlobal.FolderDocuments))
                Directory.CreateDirectory(MyGlobal.FolderDocuments);

            var file = Directory.GetFiles(MyGlobal.FolderDocuments, "*.json", SearchOption.TopDirectoryOnly);
            if (file.Length == 0)
            {
                new_document = new Document()
                {
                    NumerProjektu = "U-XXXXX",
                    NumerDokumentu = "XX-YYYYYY",
                    Rewizja = "00",
                    NumerDokumentuObcy = "XXX-YYY-ZZZ",
                    Data = DateTime.Today
                };

                var name = $"{new_document.NumerProjektu}_{new_document.NumerDokumentu}.json";
                var new_path = Path.Combine(MyGlobal.FolderDocuments, name);

                var json = JsonConvert.SerializeObject(new_document, Formatting.Indented);
                File.WriteAllText(new_path, json);
            }
        }

        #region Events
        /// <summary>
        /// Event for resizing form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mainForm_Resize(object sender, EventArgs e)
        {
            this.pictureBox1.Location = new Point(tabMain.Size.Width - this.pictureBox1.Width - tabMain.Margin.Right, tabMain.Size.Height - this.pictureBox1.Height - tabMain.Margin.Bottom);
        }

        /// <summary>
        /// Load to document from file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bLoad_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Title = "Please select file with data";
                ofd.InitialDirectory = MyGlobal.FolderDocuments;
                ofd.Filter = "Document JSON | *.json";

                var dg = ofd.ShowDialog();
                if (dg == DialogResult.OK)
                {
                    MyTools.ClearControls(listControl);
                    new_document = JsonConvert.DeserializeObject<Document>(File.ReadAllText(ofd.FileName));

                    MyTools.SetDocument(listControl, new_document);
                }
            }
        }

        /// <summary>
        /// Clear fields what they was assigned to document
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bClear_Click(object sender, EventArgs e)
        {
            new_document = null;
            MyTools.ClearControls(listControl);
        }

        /// <summary>
        /// Save document to file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bSave_Click(object sender, EventArgs e)
        {
            MyTools.GetDocument(listControl, out new_document);

            var name = $"{new_document.NumerProjektu}_{new_document.NumerDokumentu}.json";
            var new_path = Path.Combine(MyGlobal.FolderDocuments, name);

            var json = JsonConvert.SerializeObject(new_document, Formatting.Indented);
            File.WriteAllText(new_path, json);
        }

        /// <summary>
        /// Check if selected elements are from steel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkedLists_SelectedIndexChanged(object sender, EventArgs e)
        {
            var _control = (CheckedListBox)sender;
            if (_control.SelectedIndex == -1)
                return;

            var selectedText = _control.Items[_control.SelectedIndex].ToString();

            switch(selectedText)
            {
                case "STAL":
                    //var isChecked = false;
                    var isChecked = _control.GetItemChecked(_control.SelectedIndex);
                    for(int i = 0; i < MyGlobal.allowedLists.Length; i++)
                    {
                        var temp = _control.FindString(MyGlobal.TitleReports + MyGlobal.allowedLists[i]);
                        if (temp < 0)
                            continue;

                        _control.SetItemChecked(temp, isChecked);
                    }
                    break;
            }
        }

        /// <summary>
        /// Check if any environment is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void c_environment_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cbx = sender as ComboBox;
            if (cbx.SelectedIndex < 0)
                return;

            MyTools.GetTemplates(cbx, c_template);
        }

        /// <summary>
        /// Load reports file to datagrids
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _loadreports_Click(object sender, EventArgs e)
        {
            MyTools.ResetDataGrids(listGrid);

            this.tabCtrl.SelectedIndex = 0;
            using (FolderBrowserDialog folderbrowser = new FolderBrowserDialog())
            {
                folderbrowser.SelectedPath = MyGlobal.FolderReports;
                folderbrowser.Description = "Please choose folder with generated reports";
                folderbrowser.ShowNewFolderButton = false;

                var result = folderbrowser.ShowDialog();
                if (result == DialogResult.OK)
                {
                    MyGlobal.FolderInputs = folderbrowser.SelectedPath;
                    var list_inputs = new Dictionary<string, string>();

                    var found_files = Directory.GetFiles(MyGlobal.FolderInputs, "*.xml", SearchOption.TopDirectoryOnly);
                    if (found_files.Length > 0)
                    {
                        MyGlobal.FolderOutputs = folderbrowser.SelectedPath;

                        for (int i = 0; i < found_files.Length; i++)
                        {
                            var temp_file = found_files[i];

                            var tempText = "VE";
                            if (MyTools.IsMatched(temp_file, tempText))
                            {
                                if (!list_inputs.ContainsKey(tempText))
                                    MyBOM.VE(temp_file, listGrid[tempText].Grid);
                            }

                            tempText = "ST";
                            if (MyTools.IsMatched(temp_file, tempText))
                            {
                                if (!list_inputs.ContainsKey(tempText))
                                    MyBOM.ST(temp_file, listGrid[tempText].Grid);
                            }

                            tempText = "M";
                            if (MyTools.IsMatched(temp_file, tempText))
                            {
                                if (!list_inputs.ContainsKey(tempText))
                                    MyBOM.M(temp_file, listGrid[tempText].Grid);
                            }

                            tempText = "SMS_M";
                            if (MyTools.IsMatched(temp_file, tempText))
                            {
                                if (!list_inputs.ContainsKey(tempText))
                                    MyBOM.SMSM(temp_file, listGrid[tempText].Grid);
                            }

                            tempText = "SMS_W";
                            if (MyTools.IsMatched(temp_file, tempText))
                            {
                                if (!list_inputs.ContainsKey(tempText))
                                    MyBOM.SMSW(temp_file, listGrid[tempText].Grid);
                            }

                            tempText = "Kotwy";
                            if (MyTools.IsMatched(temp_file, tempText))
                            {
                                if (!list_inputs.ContainsKey(tempText))
                                    MyBOM.A(temp_file, listGrid[tempText].Grid);
                            }

                            tempText = "Single";
                            if (MyTools.IsMatched(temp_file, tempText))
                            {
                                if (!list_inputs.ContainsKey(tempText))
                                    MyBOM.S(temp_file, listGrid[tempText].Grid);
                            }
                        }
                    }

                    if (list_inputs.Count > 0)
                        this.tabCtrl.SelectedIndex = 1;
                }
            }
        }

        /// <summary>
        /// Making report from members into model
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bBOM_Click(object sender, EventArgs e)
        {
            if (!Model.GetConnectionStatus())
                return;

            MyTools.ResetDataGrids(listGrid);
            if (TeklaStuff.GetMembersFromModel().Count == 0)
            {
                MessageBox.Show("You didn't selected parts.");
                return;
            }

            MakeReports(_model, "selected");
        }
        #endregion

        /// <summary>
        /// Generating report files
        /// </summary>
        /// <param name="_model"></param>
        /// <param name="selection"></param>
        private void MakeReports(TSM.Model _model, string selection)
        {
            MyTools.GetFolderOutput(_folder_output.Text);

            var checked_reports = new List<string>();
            var items = checkedLists.CheckedItems;

            for (int i = 0; i < items.Count; i++)
            {
                var temp_report = items[i].ToString();
                var temp_file = $"{_folder_output.Text}{temp_report}";
                temp_file = Path.GetFileNameWithoutExtension(temp_file);
                temp_report = Path.GetFileNameWithoutExtension(temp_report);

                var test = selection == "selected" ? TSMO.Operation.CreateReportFromSelected(temp_report, temp_file, "", "", "") : TSMO.Operation.CreateReportFromAll(temp_report, temp_file, "", "", "");

                var old_file = Path.Combine(MyGlobal.FolderReports, temp_file);
                if (MyGlobal.FolderReports != MyGlobal.FolderOutputs)
                {
                    var new_file = Path.Combine(MyGlobal.FolderOutputs, temp_file);
                    while (File.Exists(old_file))
                    {
                        if (File.Exists(new_file))
                            File.Delete(new_file);

                        File.Move(old_file, new_file);
                        checked_reports.Add(new_file);
                    }
                }
                else checked_reports.Add(old_file);
            }

            if (checked_reports.Count == 0)
                return;

            MyTools.LoadReports(checked_reports, listGrid);
            tabCtrl.SelectedIndex = 1;
        }

        /// <summary>
        /// Get all conrols to save/load, clear operations etc.
        /// </summary>
        private void CollectControls()
        {
            listControl = new Dictionary<string, Control>()
            {
                { "project", t_project },
                { "document", t_document },
                { "phase", t_phase },
                { "rev", c_rev },
                { "object", t_object },
                { "createdby", t_createdby },
                { "checkedby", t_checkedby },
                { "contractor", t_contractor },
                { "name", t_name },
                { "date", d_date },
                { "title", t_title },
                { "subtitle", t_subtitle },
                { "document_other", t_document_other }
            };

            listGrid = new Dictionary<string, MyGrids>()
            {
                { "VE", new MyGrids { Grid = gridVE, NumberSheet = "2", IntSheet = 3, Letter = "ve" } },
                { "ST", new MyGrids { Grid = gridST, NumberSheet = "3", IntSheet = 4, Letter = "st" } },
                { "M", new MyGrids { Grid = gridM, NumberSheet = "m", IntSheet = 5, Letter = "m" } },
                { "Single", new MyGrids { Grid = gridS, NumberSheet = "7", IntSheet = 6, Letter = "s" } },
                { "SMS_M", new MyGrids { Grid = gridSMSM, NumberSheet = "4", IntSheet = 7, Letter = "smsm" } },
                { "SMS_W", new MyGrids { Grid = gridSMSW, NumberSheet = "5", IntSheet = 8, Letter = "smsw" } },
                { "Kotwy", new MyGrids { Grid = gridA, NumberSheet = "6", IntSheet = 9, Letter = "a" } },
            };

            tabCtrl.SelectedIndex = 0;
        }

        /// <summary>
        /// Writting data from datagrids to Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bExcel_Click(object sender, EventArgs e)
        {
            bool locked = true;
            try
            {
                MyTools.GetDocument(listControl, out new_document);

                if (c_template.SelectedIndex < 0)
                    throw new Exception("Proszę wybrać szablon.");

                if (new_document == null)
                    throw new Exception("Proszę wypełnić dokument.");

                if (c_rev.SelectedIndex < 0)
                    throw new Exception("Proszę wybrać rewizję.");

                var selected_environment = c_environment.Items[c_environment.SelectedIndex].ToString();
                var selected_template = c_template.Items[c_template.SelectedIndex].ToString();
                var file_template = Path.Combine(MyGlobal.FolderTemplates, selected_environment, "Steel", selected_template);

                MakeExcel(selected_environment, selected_template, file_template);

            }
            catch (Exception exc)
            {
                locked = false;
                MessageBox.Show(exc.Message, "Komunikat", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                if(locked)
                    MessageBox.Show("Zrobione", "Komunikat", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Doing in Excel and saving
        /// </summary>
        /// <param name="selected_environment"></param>
        /// <param name="selected_template"></param>
        /// <param name="newFile"></param>
        private void MakeExcel(string selected_environment, string selected_template, string selected_file)
        {
            MyTools.GetLanguage(selected_template);
            MyStuff.tempDocument = new_document;

            FileInfo openFile = new FileInfo(selected_file);
            using (ExcelPackage pkg = new ExcelPackage(openFile))
            {
                var book = pkg.Workbook;

                MyExcel.SetData(book);

                for (int i = listGrid.Count; i > 0; i--)
                {
                    var tempElement = listGrid.ElementAt(i-1);
                    var tempGrid = tempElement.Value.Grid.DataSource;
                    var tempSheet = tempElement.Value.NumberSheet;
                    var tempIndex = tempElement.Value.IntSheet;

                    if (tempGrid != null)
                    {
                        MyExcel.Data(book, tempSheet);
                        switch(tempElement.Key)
                        {
                            case "VE":
                                MyExcel.DataVE(book, tempElement);
                                break;
                            case "ST":
                                MyExcel.DataST(book, tempElement);
                                break;
                            case "M":
                                MyExcel.DataM(book, tempElement);
                                break;
                            case "Single":
                                MyExcel.DataS(book, tempElement);
                                break;
                            case "SMS_M":
                                MyExcel.DataSMSM(book, tempElement);
                                break;
                            case "SMS_W":
                                MyExcel.DataSMSW(book, tempElement);
                                break;
                            case "Kotwy":
                                MyExcel.DataA(book, tempElement);
                                break;
                        }
                    }
                    else book.Worksheets.Delete(tempIndex);
                }

                var default_file = Path.Combine(MyGlobal.FolderOutputs, $"{new_document.NumerDokumentu}_{new_document.Rewizja}.xlsx");
                var saveAs = new FileInfo(default_file);
                pkg.SaveAs(saveAs);
            }
        }

        /// <summary>
        /// Showing revisions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void b_rev_Click(object sender, EventArgs e)
        {
            string file = Path.Combine(MyGlobal.AppPath, "revisions.txt");
            if (File.Exists(file))
            {
                Revisions form = new Revisions(file);
                var dg = form.ShowDialog();
                if (dg == DialogResult.OK)
                    MyTools.LoadRevisions(MyGlobal.AppPath, c_rev);
            }
        }

        /// <summary>
        /// Opening folder with outputs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bOpen_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(MyGlobal.FolderOutputs))
                return;

            System.Diagnostics.Process.Start(MyGlobal.FolderOutputs);
        }
    }
}
