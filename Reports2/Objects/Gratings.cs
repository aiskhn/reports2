﻿using System;

namespace Reports2
{
    //inherite IEquatable
    internal class Gratings : IEquatable<Gratings>
    {
        public string antykorozyjne { get; internal set; }
        public int count { get; internal set; }
        public string gratingCount { get; internal set; }
        public int ilosc { get; internal set; }
        public string kolnierz { get; internal set; }
        public string laczniki { get; internal set; }
        public string przeciwposlizgowe { get; internal set; }
        public string wykonanie { get; internal set; }
        public string wykonczenie { get; internal set; }

        //overriding Equals
        public bool Equals(Gratings other)
        {
            return this.antykorozyjne == other.antykorozyjne &&
                   this.gratingCount == other.gratingCount &&
                   this.kolnierz == other.kolnierz &&
                   this.przeciwposlizgowe == other.przeciwposlizgowe &&
                   this.wykonanie == other.wykonanie &&
                   this.wykonczenie == other.wykonczenie &&
                   this.laczniki == other.laczniki;
        }
    }
}