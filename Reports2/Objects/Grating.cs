﻿namespace Reports2
{
    internal class MyGrating
    {
        public MyGrating()
        {
        }

        public string anchor1 { get; set; }
        public string anchor2 { get; set; }
        public string anticorrosive { get; set; }
        public string clamp { get; set; }
        public string connector1 { get; set; }
        public string connector2 { get; set; }
        public string connector3 { get; set; }
        public string connector4 { get; set; }
        public string connector5 { get; set; }
        public string connector6 { get; set; }
        public string connector7 { get; set; }
        public string connector8 { get; set; }
        public string connector9 { get; set; }
        public string finishing1 { get; set; }
        public string finishing2 { get; set; }
        public string grating1 { get; set; }
        public string grating2 { get; set; }
        public string material { get; set; }
        public string production1 { get; set; }
        public string production2 { get; set; }
        public string quantity { get; set; }
        public string serrated1 { get; set; }
        public string serrated2 { get; set; }
        public string step1 { get; set; }
        public string step2 { get; set; }
    }
}