﻿using System.Windows.Forms;

namespace Reports2
{
    internal class MyGrids
    {
        public DataGridView Grid { get; set; }
        public int IntSheet { get; set; }
        public string Letter { get; internal set; }
        public string NumberSheet { get; set; }
    }
}