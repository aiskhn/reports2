﻿using System;

namespace Reports2
{
    public class Document
    {
        public string NumerProjektu { get; set; }
        public string NumerDokumentu { get; set; }
        public string Faza { get; set; }
        public string Rewizja { get; set; }
        public string Obiekt { get; set; }
        public string Projektant { get; set; }
        public string Konstruktor { get; set; }
        public string Zamawiajacy { get; set; }
        public string Nazwa { get; set; }
        public DateTime Data { get; set; }
        public string Tytul { get; set; }
        public string Podtytul { get; set; }
        public string NumerDokumentuObcy { get; set; }
    }
}
