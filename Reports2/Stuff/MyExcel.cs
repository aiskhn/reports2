﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Reports2
{
    /// <summary>
    /// Excel - using library EPPLus
    /// </summary>
    static class MyExcel
    {
        /// <summary>
        /// Setting data per sheet
        /// </summary>
        /// <param name="book"></param>
        /// <param name="tempSheet"></param>
        internal static void Data(ExcelWorkbook book, string tempSheet)
        {
            book.Names[$"nr_projektu_{tempSheet}"].Value = MyStuff.tempDocument.NumerProjektu;
            book.Names[$"rewizja_{tempSheet}"].Value = MyStuff.tempDocument.Rewizja;
            book.Names[$"data_{tempSheet}"].Value = MyStuff.tempDocument.Data;
            book.Names[$"zamawiajacy_{tempSheet}"].Value = MyStuff.tempDocument.Zamawiajacy;
            book.Names[$"projektant_{tempSheet}"].Value = MyStuff.tempDocument.Projektant;
            book.Names[$"nazwa_{tempSheet}"].Value = MyStuff.tempDocument.Nazwa;
            book.Names[$"obiekt_{tempSheet}"].Value = MyStuff.tempDocument.Obiekt;
            book.Names[$"konstruktor_{tempSheet}"].Value = MyStuff.tempDocument.Konstruktor;
            book.Names[$"faza_{tempSheet}"].Value = MyStuff.tempDocument.Faza;

            var isNewNumber = book.Names.ContainsKey($"projekt_obcy_{tempSheet}");
            if (isNewNumber)
                book.Names[$"projekt_obcy_{tempSheet}"].Value = MyStuff.tempDocument.NumerDokumentuObcy;
        }

        /// <summary>
        /// Setting data in main sheet
        /// </summary>
        /// <param name="book"></param>
        internal static void SetData(ExcelWorkbook book)
        {
            book.Names["projekt"].Value = MyStuff.tempDocument.NumerProjektu;
            book.Names["dokument"].Value = MyStuff.tempDocument.NumerDokumentu;
            book.Names["rewizja"].Value = MyStuff.tempDocument.Rewizja;
            book.Names["tytul"].Value = MyStuff.tempDocument.Tytul;
            book.Names["podtytul"].Value = MyStuff.tempDocument.Podtytul;
            book.Names["zamawiajacy"].Value = MyStuff.tempDocument.Zamawiajacy;
            book.Names["obiekt"].Value = MyStuff.tempDocument.Obiekt;

            book.Names["projekt_uw"].Value = MyStuff.tempDocument.NumerProjektu;
            book.Names["dokument_uw"].Value = MyStuff.tempDocument.NumerDokumentu;
            book.Names["rewizja_uw"].Value = MyStuff.tempDocument.Rewizja;

            book.Names[$"masa_etapu"].Value = MyGlobal.W_VE;
        }

        /// <summary>
        /// Filling data in sheet S
        /// </summary>
        /// <param name="book"></param>
        /// <param name="tempElement"></param>
        internal static void DataS(ExcelWorkbook book, KeyValuePair<string, MyGrids> tempElement)
        {
            var tempName = tempElement.Value.Letter;
            var tempSheet = tempElement.Value.IntSheet;
            var tempLetterSheet = tempElement.Value.NumberSheet;
            var tempDataSource = tempElement.Value.Grid.DataSource;

            var start = new ExcelAddress(book.Names[$"left_{tempName}"].Address);
            var right = new ExcelAddress(book.Names[$"right_{tempName}"].Address);
            var end = new ExcelAddress(book.Names[$"end_{tempName}"].Address);

            var col_st = start.Start.Column;
            var col_end = right.Start.Column;

            var row_st = start.Start.Row;
            var row_end = end.Start.Row;

            var _sheet = book.Worksheets[tempSheet];
            var _table = (DataTable)tempDataSource;

            var rows = _table.Rows.Count;
            for (var i = 0; i < rows; i++)
            {
                var _temp_row = row_st + i;
                var inputs = new List<object[]>()
                {
                    new object[]
                    {
                        _table.Rows[i][0].ToString(),
                        _table.Rows[i][1].ToString(),
                        _table.Rows[i][2].ToString(),
                        _table.Rows[i][3].ToString(),
                        _table.Rows[i][4].ToString(),
                        _table.Rows[i][5].ToString(),
                        _table.Rows[i][6].ToString(),
                        _table.Rows[i][7].ToString(),
                        _table.Rows[i][8].ToString(),
                        _table.Rows[i][9].ToString(),
                    }
                };

                _sheet.Cells[_temp_row, col_st, _temp_row, col_end].LoadFromArrays(inputs);

                var range = _sheet.Cells[_temp_row, col_st, _temp_row, col_end];
                //if (_table.Rows[i][9].ToString() == "T")
                //{
                //    range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                //    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                //}
            }

            var border = _sheet.Cells[row_st, col_st, row_st + (rows - 1), col_end].Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            var nr = row_st + rows;

            var ranga = _sheet.Cells[nr, col_st, nr, col_end - 4];
            ranga.Merge = true;
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            ranga.Value = MyGlobal.langTotal[MyGlobal.Language];

            ranga = _sheet.Cells[nr, col_end - 3, nr, col_end];
            ranga.Merge = true;
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            ranga.Value = MyGlobal.W_S;

            //ranga = _sheet.Cells[nr, col_end - 1];
            //ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            //ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //ranga.Value = MyGlobal.A_ST;

            //ranga = _sheet.Cells[nr, col_end];
            //ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            //ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            book.Names[$"masa_{tempLetterSheet}"].Value = MyGlobal.W_S;
        }

        /// <summary>
        /// Filling data in sheet A
        /// </summary>
        /// <param name="book"></param>
        /// <param name="tempElement"></param>
        internal static void DataA(ExcelWorkbook book, KeyValuePair<string, MyGrids> tempElement)
        {
            var tempName = tempElement.Value.Letter;
            var tempSheet = tempElement.Value.IntSheet;
            var tempLetterSheet = tempElement.Value.NumberSheet;
            var tempDataSource = tempElement.Value.Grid.DataSource;

            var extraColumn = book.Names.ContainsKey("extraColumn") ? 1 : 0;

            var start = new ExcelAddress(book.Names[$"left_{tempName}"].Address);
            var right = new ExcelAddress(book.Names[$"right_{tempName}"].Address);
            var end = new ExcelAddress(book.Names[$"end_{tempName}"].Address);

            var col_st = start.Start.Column;
            var col_end = right.Start.Column;

            var row_st = start.Start.Row;
            var row_end = end.Start.Row;

            var _sheet = book.Worksheets[tempSheet];
            var _table = (DataTable)tempDataSource;

            var rows = _table.Rows.Count;
            for (var i = 0; i < rows; i++)
            {
                var _temp_row = row_st + i;

                var inputs = new List<object[]>()
                {
                    new object[]
                    {
                        _table.Rows[i][0].ToString(),
                        _table.Rows[i][1].ToString(),
                        _table.Rows[i][2].ToString(),
                        _table.Rows[i][3].ToString(),
                        _table.Rows[i][4].ToString(),
                        _table.Rows[i][5].ToString()
                    }
                };

                if(extraColumn > 0)
                    _sheet.Cells[_temp_row, col_end - extraColumn, _temp_row, col_end].Merge = true;

                _sheet.Cells[_temp_row, col_st, _temp_row, col_end].LoadFromArrays(inputs);

            }

            var border = _sheet.Cells[row_st, col_st, row_st + (rows - 1), col_end].Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            var nr = row_st + rows;

            var ranga = _sheet.Cells[nr, col_st];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            ranga.Value = MyGlobal.langTotal[MyGlobal.Language];

            ranga = _sheet.Cells[nr, col_st + 1, nr, col_end];
            ranga.Merge = true;
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            ranga.Value = MyGlobal.C_A;

            book.Names[$"masa_{tempLetterSheet}"].Value = MyGlobal.C_A;
        }

        /// <summary>
        /// Filling data in sheet SMS W (Bolt workshop)
        /// </summary>
        /// <param name="book"></param>
        /// <param name="tempElement"></param>
        internal static void DataSMSW(ExcelWorkbook book, KeyValuePair<string, MyGrids> tempElement)
        {
            var tempName = tempElement.Value.Letter;
            var tempSheet = tempElement.Value.IntSheet;
            var tempLetterSheet = tempElement.Value.NumberSheet;
            var tempDataSource = tempElement.Value.Grid.DataSource;

            var start = new ExcelAddress(book.Names[$"left_{tempName}"].Address);
            var right = new ExcelAddress(book.Names[$"right_{tempName}"].Address);
            var end = new ExcelAddress(book.Names[$"end_{tempName}"].Address);

            var col_st = start.Start.Column;
            var col_end = right.Start.Column;

            var row_st = start.Start.Row;
            var row_end = end.Start.Row;

            var _sheet = book.Worksheets[tempSheet];
            var _table = (DataTable)tempDataSource;

            var rows = _table.Rows.Count;
            for (var i = 0; i < rows; i++)
            {
                var _temp_row = row_st + i;

                var name_to_change = _table.Rows[i][2].ToString();
                if (new Regex("Nut").Match(name_to_change).Success)
                    name_to_change = name_to_change.Replace("Nut", MyGlobal.langNut[MyGlobal.Language]);

                if (new Regex("Washer").Match(name_to_change).Success)
                    name_to_change = name_to_change.Replace("Washer", MyGlobal.langWasher[MyGlobal.Language]);

                var standard_to_change = _table.Rows[i][3].ToString();
                if (new Regex("DUMMY").Match(standard_to_change).Success)
                    standard_to_change = standard_to_change.Replace(standard_to_change, "-");

                var inputs = new List<object[]>()
                {
                    new object[]
                    {
                        _table.Rows[i][0].ToString(),
                        _table.Rows[i][1].ToString(),
                        name_to_change,
                        standard_to_change,
                        _table.Rows[i][4].ToString(),
                        _table.Rows[i][5].ToString(),
                        _table.Rows[i][6].ToString(),
                        _table.Rows[i][7].ToString()
                    }
                };

                //_sheet.Cells[_temp_row, col_end - 1, _temp_row, col_end].Merge = true;
                _sheet.Cells[_temp_row, col_st, _temp_row, col_end].LoadFromArrays(inputs);

                var range = _sheet.Cells[_temp_row, col_st, _temp_row, col_end];
                if (_table.Rows[i][8].ToString() == "T")
                {
                    range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                }
            }

            var border = _sheet.Cells[row_st, col_st, row_st + (rows - 1), col_end].Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            var nr = row_st + rows;

            var ranga = _sheet.Cells[nr, col_st, nr, col_end - 2];
            ranga.Merge = true;
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            ranga.Value = MyGlobal.langTotal[MyGlobal.Language];

            ranga = _sheet.Cells[nr, col_end - 1];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Value = MyGlobal.W_SMSW;

            ranga = _sheet.Cells[nr, col_end];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            book.Names[$"masa_{tempLetterSheet}"].Value = MyGlobal.W_SMSW;
        }

        /// <summary>
        /// Filling data in sheet SMS M (Bolt site)
        /// </summary>
        /// <param name="book"></param>
        /// <param name="tempElement"></param>
        internal static void DataSMSM(ExcelWorkbook book, KeyValuePair<string, MyGrids> tempElement)
        {
            var tempName = tempElement.Value.Letter;
            var tempSheet = tempElement.Value.IntSheet;
            var tempLetterSheet = tempElement.Value.NumberSheet;
            var tempDataSource = tempElement.Value.Grid.DataSource;

            var start = new ExcelAddress(book.Names[$"left_{tempName}"].Address);
            var right = new ExcelAddress(book.Names[$"right_{tempName}"].Address);
            var end = new ExcelAddress(book.Names[$"end_{tempName}"].Address);

            var col_st = start.Start.Column;
            var col_end = right.Start.Column;

            var row_st = start.Start.Row;
            var row_end = end.Start.Row;

            var _sheet = book.Worksheets[tempSheet];
            var _table = (DataTable)tempDataSource;

            var rows = _table.Rows.Count;
            for (var i = 0; i < rows; i++)
            {
                var _temp_row = row_st + i;

                var name_to_change = _table.Rows[i][2].ToString();
                if (new Regex("Nut").Match(name_to_change).Success)
                    name_to_change = name_to_change.Replace("Nut", MyGlobal.langNut[MyGlobal.Language]);

                if (new Regex("Washer").Match(name_to_change).Success)
                    name_to_change = name_to_change.Replace("Washer", MyGlobal.langWasher[MyGlobal.Language]);

                var standard_to_change = _table.Rows[i][3].ToString();
                if (new Regex("DUMMY").Match(standard_to_change).Success)
                    standard_to_change = standard_to_change.Replace(standard_to_change, "-");

                var inputs = new List<object[]>()
                {
                    new object[]
                    {
                        _table.Rows[i][0].ToString(),
                        _table.Rows[i][1].ToString(),
                        name_to_change,
                        standard_to_change,
                        _table.Rows[i][4].ToString(),
                        _table.Rows[i][5].ToString(),
                        _table.Rows[i][6].ToString(),
                        _table.Rows[i][7].ToString()
                    }
                };

                _sheet.Cells[_temp_row, col_st, _temp_row, col_end].LoadFromArrays(inputs);

                var range = _sheet.Cells[_temp_row, col_st, _temp_row, col_end];
                if (_table.Rows[i][8].ToString() == "T")
                {
                    range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                }
            }

            var border = _sheet.Cells[row_st, col_st, row_st + (rows - 1), col_end].Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            var nr = row_st + rows;

            var ranga = _sheet.Cells[nr, col_st, nr, col_end - 2];
            ranga.Merge = true;
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            ranga.Value = MyGlobal.langTotal[MyGlobal.Language];

            ranga = _sheet.Cells[nr, col_end - 1];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Value = MyGlobal.W_SMSM;

            ranga = _sheet.Cells[nr, col_end];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            book.Names[$"masa_{tempLetterSheet}"].Value = MyGlobal.W_SMSM;
        }

        /// <summary>
        /// Filling data in sheet M
        /// </summary>
        /// <param name="book"></param>
        /// <param name="tempElement"></param>
        internal static void DataM(ExcelWorkbook book, KeyValuePair<string, MyGrids> tempElement)
        {
            var tempName = tempElement.Value.Letter;
            var tempSheet = tempElement.Value.IntSheet;
            var tempLetterSheet = tempElement.Value.NumberSheet;
            var tempDataSource = tempElement.Value.Grid.DataSource;

            var start = new ExcelAddress(book.Names[$"left_{tempName}"].Address);
            var right = new ExcelAddress(book.Names[$"right_{tempName}"].Address);
            var end = new ExcelAddress(book.Names[$"end_{tempName}"].Address);

            var col_st = start.Start.Column;
            var col_end = right.Start.Column;

            var row_st = start.Start.Row;
            var row_end = end.Start.Row;

            var _sheet = book.Worksheets[tempSheet];
            var _table = (DataTable)tempDataSource;

            var rows = _table.Rows.Count;
            for (var i = 0; i < rows; i++)
            {
                var _temp_row = row_st + i;
                var inputs = new List<object[]>()
                {
                    new object[]
                    {
                        _table.Rows[i][0].ToString(),
                        _table.Rows[i][1].ToString(),
                        _table.Rows[i][2].ToString(),
                        _table.Rows[i][3].ToString(),
                        _table.Rows[i][4].ToString(),
                        _table.Rows[i][5].ToString(),
                        _table.Rows[i][6].ToString(),
                        _table.Rows[i][7].ToString(),
                        _table.Rows[i][8].ToString()
                    }
                };

                _sheet.Cells[_temp_row, col_st, _temp_row, col_end].LoadFromArrays(inputs);

                var range = _sheet.Cells[_temp_row, col_st, _temp_row, col_end];
                if (_table.Rows[i][0].ToString() == "SUMMARY" || _table.Rows[i][0].ToString() == "SUMA")
                {
                    range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range = _sheet.Cells[_temp_row, 1, _temp_row, 1];
                    range.Value = MyGlobal.langSummary[MyGlobal.Language];
                }
                else if (_table.Rows[i][0].ToString() == "P")
                {
                    _sheet.Cells[_temp_row, col_st, _temp_row, col_st].Value = _table.Rows[i][1].ToString();
                    range = _sheet.Cells[_temp_row, col_st, _temp_row, col_end];
                    range.Merge = true;
                    range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    range.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Bold = true;
                }
                else if (_table.Rows[i][0].ToString() == "NEWLINE")
                {
                    _sheet.Cells[_temp_row, col_st, _temp_row, col_st].Value = "";
                    range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.None;
                    range = _sheet.Cells[_temp_row, col_st, _temp_row, col_end];
                    range.Merge = true;
                }
            }

            var border = _sheet.Cells[row_st, col_st, row_st + (rows - 1), col_end].Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            var nr = row_st + rows;

            var ranga = _sheet.Cells[nr, col_st, nr, col_end - 5];
            ranga.Merge = true;
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            ranga.Value = MyGlobal.langTotal[MyGlobal.Language];

            ranga = _sheet.Cells[nr, col_end - 4];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Value = MyGlobal.L_M;

            ranga = _sheet.Cells[nr, col_end - 3];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Value = "";

            ranga = _sheet.Cells[nr, col_end - 2];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Value = MyGlobal.W_M;

            ranga = _sheet.Cells[nr, col_end - 1];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Value = MyGlobal.A_M;

            ranga = _sheet.Cells[nr, col_end];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            book.Names[$"masa_{tempLetterSheet}"].Value = MyGlobal.W_M;
        }

        /// <summary>
        /// Filling data in sheet ST
        /// </summary>
        /// <param name="book"></param>
        /// <param name="tempElement"></param>
        internal static void DataST(ExcelWorkbook book, KeyValuePair<string, MyGrids> tempElement)
        {
            var tempName = tempElement.Value.Letter;
            var tempSheet = tempElement.Value.IntSheet;
            var tempLetterSheet = tempElement.Value.NumberSheet;
            var tempDataSource = tempElement.Value.Grid.DataSource;

            var start = new ExcelAddress(book.Names[$"left_{tempName}"].Address);
            var right = new ExcelAddress(book.Names[$"right_{tempName}"].Address);
            var end = new ExcelAddress(book.Names[$"end_{tempName}"].Address);

            var col_st = start.Start.Column;
            var col_end = right.Start.Column;

            var row_st = start.Start.Row;
            var row_end = end.Start.Row;

            var _sheet = book.Worksheets[tempSheet];
            var _table = (DataTable)tempDataSource;

            var rows = _table.Rows.Count;
            for (var i = 0; i < rows; i++)
            {
                var _temp_row = row_st + i;
                var inputs = new List<object[]>()
                {
                    new object[]
                    {
                        _table.Rows[i][0].ToString(),
                        _table.Rows[i][1].ToString(),
                        _table.Rows[i][2].ToString(),
                        _table.Rows[i][3].ToString(),
                        _table.Rows[i][4].ToString(),
                        _table.Rows[i][5].ToString(),
                        _table.Rows[i][6].ToString(),
                        _table.Rows[i][7].ToString(),
                        _table.Rows[i][8].ToString()
                    }
                };

                _sheet.Cells[_temp_row, col_st, _temp_row, col_end].LoadFromArrays(inputs);

                var range = _sheet.Cells[_temp_row, col_st, _temp_row, col_end];
                if (_table.Rows[i][9].ToString() == "T")
                {
                    range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                }
            }

            var border = _sheet.Cells[row_st, col_st, row_st + (rows - 1), col_end].Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            var nr = row_st + rows;

            var ranga = _sheet.Cells[nr, col_st, nr, col_end - 3];
            ranga.Merge = true;
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            ranga.Value = MyGlobal.langTotal[MyGlobal.Language];

            ranga = _sheet.Cells[nr, col_end - 2];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Value = MyGlobal.W_ST;

            ranga = _sheet.Cells[nr, col_end - 1];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Value = MyGlobal.A_ST;

            ranga = _sheet.Cells[nr, col_end];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            book.Names[$"masa_{tempLetterSheet}"].Value = MyGlobal.W_ST;
        }

        /// <summary>
        /// Filling data in sheet VE
        /// </summary>
        /// <param name="book"></param>
        /// <param name="tempElement"></param>
        internal static void DataVE(ExcelWorkbook book, KeyValuePair<string, MyGrids> tempElement)
        {
            var tempName = tempElement.Value.Letter;
            var tempSheet = tempElement.Value.IntSheet;
            var tempLetterSheet = tempElement.Value.NumberSheet;
            var tempDataSource = tempElement.Value.Grid.DataSource;

            var getGratings = MyTools.GetGratingsBy((DataTable)tempDataSource);

            var start = new ExcelAddress(book.Names[$"left_{tempName}"].Address);
            var right = new ExcelAddress(book.Names[$"right_{tempName}"].Address);
            var end = new ExcelAddress(book.Names[$"end_{tempName}"].Address);

            var col_st = start.Start.Column;
            var col_end = right.Start.Column;

            var row_st = start.Start.Row;
            var row_end = end.Start.Row;

            var _sheet = book.Worksheets[tempSheet];
            var _table = (DataTable)tempDataSource;

            int available_rows = row_end - row_st;

            var rows = _table.Rows.Count;
            for (var i = 0; i < rows; i++)
            {
                var _temp_row = row_st + i;
                var inputs = new List<object[]>()
                {
                    new object[]
                    {
                        _table.Rows[i][0].ToString(),
                        _table.Rows[i][1].ToString(),
                        _table.Rows[i][2].ToString(),
                        _table.Rows[i][3].ToString(),
                        _table.Rows[i][4].ToString(),
                        _table.Rows[i][5].ToString(),
                        _table.Rows[i][6].ToString(),
                        _table.Rows[i][7].ToString()
                    }
                };

                _sheet.Cells[_temp_row, col_st, _temp_row, col_end].LoadFromArrays(inputs);
            }

            var border = _sheet.Cells[row_st, col_st, row_st + (rows - 1), col_end].Style.Border;
            border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            var nr = row_st + rows;

            var ranga = _sheet.Cells[nr, col_st, nr, col_end - 3];
            ranga.Merge = true;
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            ranga.Value = MyGlobal.langTotal[MyGlobal.Language];

            ranga = _sheet.Cells[nr, col_end - 2];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Value = MyGlobal.W_VE;

            ranga = _sheet.Cells[nr, col_end - 1];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            ranga.Value = MyGlobal.A_VE;

            ranga = _sheet.Cells[nr, col_end];
            ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
            ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            book.Names[$"masa_{tempLetterSheet}"].Value = MyGlobal.W_VE;

            var rest = rows % available_rows;
            var free_space = 23;
            if (getGratings.Count > 0)
            {
                if (rest >= free_space)
                {
                    var multiple = (rows / available_rows) + 1;
                    var new_position = (multiple * available_rows) + row_st;
                    nr = new_position;
                }

                AddDescriptiotnGR(nr + 2, col_end, _sheet, ranga, getGratings);
            }
        }

        /// <summary>
        /// Adding extra description about grating plates
        /// </summary>
        /// <param name="number_row"></param>
        /// <param name="col_end"></param>
        /// <param name="_sheet"></param>
        /// <param name="ranga"></param>
        /// <param name="listGratings"></param>
        internal static void AddDescriptiotnGR(int number_row, int col_end, ExcelWorksheet _sheet, ExcelRange ranga, List<Gratings> listGratings)
        {
            var temp_row = 0;

            var countGratings = listGratings.Count;
            var sortedGratings = listGratings.OrderBy(o => o.gratingCount).ToList();

            for (var i = 0; i < sortedGratings.Count; i++)
            {
                var tempSortedGrating = sortedGratings[i];

                ranga = _sheet.Cells[number_row + temp_row, 1, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
                ranga.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ranga.Style.Font.Size = 12;
                ranga.Style.Font.Name = "Arial";
                ranga.Style.Font.Bold = true;
                ranga.Value = MyGlobal.langGrating[MyGlobal.Language].grating1;
                _sheet.Row(number_row + temp_row).Height = 18.00;
                temp_row++;

                //#1
                ranga = _sheet.Cells[number_row + temp_row, 1];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ranga.Value = "1";

                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = MyGlobal.langGrating[MyGlobal.Language].grating2;
                ranga.Style.WrapText = true;
                _sheet.Row(number_row + temp_row).Height = _sheet.Row(number_row + temp_row).Height * 3;
                temp_row++;

                //#2
                ranga = _sheet.Cells[number_row + temp_row, 1];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = "2";

                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = tempSortedGrating.wykonanie;
                temp_row++;

                //#3
                ranga = _sheet.Cells[number_row + temp_row, 1];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = "3";

                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = tempSortedGrating.wykonczenie;
                temp_row++;

                ////#4
                ranga = _sheet.Cells[number_row + temp_row, 1];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = "4";

                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = tempSortedGrating.przeciwposlizgowe;
                temp_row++;

                //#5
                ranga = _sheet.Cells[number_row + temp_row, 1];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = "5";

                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = MyGlobal.langGrating[MyGlobal.Language].material;
                temp_row++;

                //#6
                ranga = _sheet.Cells[number_row + temp_row, 1];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = "6";

                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = MyGlobal.langGrating[MyGlobal.Language].anticorrosive;
                temp_row++;

                //#7
                ranga = _sheet.Cells[number_row + temp_row, 1, number_row + temp_row + 1, 1];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ranga.Value = "7";

                //7.1
                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = MyGlobal.langGrating[MyGlobal.Language].connector1;
                temp_row++;

                //7.2
                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end - 2];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Style.WrapText = true;
                ranga.Value = tempSortedGrating.laczniki;

                ranga = _sheet.Cells[number_row + temp_row, col_end - 1];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Style.Numberformat.Format = "# ##0";
                int iloraz_kratek = Convert.ToInt32(tempSortedGrating.gratingCount) * tempSortedGrating.ilosc;// 4 * gratings_count;
                ranga.Value = iloraz_kratek.ToString();

                ranga = _sheet.Cells[number_row + temp_row, col_end];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = MyGlobal.langGrating[MyGlobal.Language].quantity;
                temp_row++;

                //// 7.3 nowy
                //ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end - 2];
                //ranga.Merge = true;
                //ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                //ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                //ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                //ranga.Value = tempSortedGrating.kolnierz;

                //ranga = _sheet.Cells[number_row + temp_row, col_end - 1];
                //ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                //ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                //ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                //ranga.Value = "-";

                //ranga = _sheet.Cells[number_row + temp_row, col_end];
                //ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                //ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                //ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                //ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                //ranga.Value = MyGlobal.langGrating[MyGlobal.Language].quantity;
                //temp_row++;

                ////7.4
                //ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end - 2];
                //ranga.Merge = true;
                //ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                //ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                //ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                //ranga.Value = MyGlobal.langGrating[MyGlobal.Language].clamp;

                //ranga = _sheet.Cells[number_row + temp_row, col_end - 1];
                //ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                //ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                //ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                //ranga.Value = "-";

                //ranga = _sheet.Cells[number_row + temp_row, col_end];
                //ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                //ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                //ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                //ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                //ranga.Value = MyGlobal.langGrating[MyGlobal.Language].quantity;
                //temp_row++;

                //UWAGI: STOPNIE
                ranga = _sheet.Cells[number_row + temp_row, 1, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ranga.Style.Font.Size = 12;
                ranga.Style.Font.Name = "Arial";
                ranga.Style.Font.Bold = true;
                ranga.Value = MyGlobal.langGrating[MyGlobal.Language].step1;//"REMARKS: STEPS";
                _sheet.Row(number_row + temp_row).Height = 18.00;
                temp_row++;

                //#1
                ranga = _sheet.Cells[number_row + temp_row, 1];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ranga.Value = "1";

                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = tempSortedGrating.wykonanie;//MyGlobal.temp1;
                temp_row++;

                //#2
                ranga = _sheet.Cells[number_row + temp_row, 1];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ranga.Value = "2";

                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = tempSortedGrating.przeciwposlizgowe;
                temp_row++;

                //#3
                ranga = _sheet.Cells[number_row + temp_row, 1];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ranga.Value = "3";

                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = MyGlobal.langGrating[MyGlobal.Language].step2;
                temp_row++;

                //#4
                ranga = _sheet.Cells[number_row + temp_row, 1];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ranga.Value = "4";

                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = MyGlobal.langGrating[MyGlobal.Language].material;
                temp_row++;

                //#5
                ranga = _sheet.Cells[number_row + temp_row, 1];
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ranga.Value = "5";

                ranga = _sheet.Cells[number_row + temp_row, 2, number_row + temp_row, col_end];
                ranga.Merge = true;
                ranga.Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                ranga.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ranga.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                ranga.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                ranga.Value = MyGlobal.langGrating[MyGlobal.Language].anticorrosive;

                temp_row += 2;
            }
        }
    }
}
