﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using TS = Tekla.Structures;

namespace Reports2
{
    internal class MyStuff
    {
        internal static Document tempDocument { get; set; }
    }

    internal class MyTools
    {
        /// <summary>
        /// Getting information about gratings to be placed in extra description for this element
        /// </summary>
        /// <param name="_output"></param>
        /// <returns></returns>
        internal static List<Gratings> GetGratingsBy(DataTable _output)
        {
            var listGratings = new List<Gratings>();

            var divideRows = _output.Rows.Count / 2;
            for (var i = 0; i < divideRows; i++)
            {
                var tempRow = _output.Rows[i * 2];
                var tempCount = 0;

                if (Convert.ToInt32((string)tempRow["gratingCount"]) > 0)
                {
                    var tempGrating = new Gratings
                    {
                        gratingCount = tempRow["gratingCount"].ToString(),
                        wykonanie = tempRow["wykonanie"].ToString(),
                        wykonczenie = tempRow["wykonczenie"].ToString(),
                        przeciwposlizgowe = tempRow["przeciwposlizgowe"].ToString(),
                        antykorozyjne = tempRow["antykorozyjne"].ToString(),
                        kolnierz = tempRow["kolnierz"].ToString(),
                        laczniki = tempRow["laczniki"].ToString(),
                        ilosc = Convert.ToInt32(tempRow["Szt."].ToString()),
                        count = tempCount + 1
                    };

                    if (!listGratings.Contains(tempGrating))
                        listGratings.Add(tempGrating);
                    else
                    {
                        //use overrided Equals for class Gratings
                        var foundIndex = listGratings.FindIndex(item => item.Equals(tempGrating));
                        if(foundIndex > -1)
                            listGratings[foundIndex].count += 1;
                    }
                }
            }

            return listGratings;
        }

        /// <summary>
        /// Getting settings from file config
        /// </summary>
        internal static void GetConfigurationValue()
        {
            var company = ConfigurationManager.AppSettings["company"];
            var shortcut = ConfigurationManager.AppSettings["shortcut"];
            var language = ConfigurationManager.AppSettings["language"];

            MyGlobal.defComp = company;
            MyGlobal.defLang = language;
            MyGlobal.defTitleRep = shortcut;

            MyGlobal.SetFolderDocuments();
        }

        /// <summary>
        /// Loadinv revisions from file text
        /// </summary>
        /// <param name="appPath"></param>
        /// <param name="c_rev"></param>
        internal static void LoadRevisions(string appPath, ComboBox c_rev)
        {
            c_rev.Items.Clear();

            var file_revision = Path.Combine(appPath, "revisions.txt");
            if (File.Exists(file_revision))
            {
                var revisions = File.ReadAllLines(file_revision, Encoding.UTF8);
                c_rev.Items.AddRange(revisions.ToArray());
            }

            if (c_rev.Items.Count > 0)
                c_rev.SelectedIndex = 0;
        }

        /// <summary>
        /// Matching file report
        /// </summary>
        /// <param name="temp_file"></param>
        /// <param name="tempText"></param>
        /// <returns></returns>
        internal static bool IsMatched(string temp_file, string tempText)
        {
            return new Regex($"{MyGlobal.TitleReports}{tempText}").Match(temp_file).Success;
        }

        /// <summary>
        /// Getting folder output for reports
        /// </summary>
        /// <param name="text"></param>
        internal static void GetFolderOutput(string text)
        {
            if (text != "")
            {
                MyGlobal.FolderOutputs = Path.Combine(MyGlobal.FolderReports, text);
                if (!Directory.Exists(MyGlobal.FolderOutputs))
                    Directory.CreateDirectory(MyGlobal.FolderOutputs);
            }
        }

        /// <summary>
        /// Generate list of reports
        /// </summary>
        /// <returns></returns>
        internal static List<string> GenerateElements()
        {
            return new List<string>(new string[] {
                MyGlobal.TitleReports + "VE",
                MyGlobal.TitleReports + "ST",
                MyGlobal.TitleReports + "M",
                MyGlobal.TitleReports + "SMS_M",
                MyGlobal.TitleReports + "SMS_W",
                MyGlobal.TitleReports + "Kotwy",
                MyGlobal.TitleReports + "Single"
            });
        }

        /// <summary>
        /// Check if any reports are from steel
        /// </summary>
        /// <param name="checked_reports"></param>
        /// <param name="elements_steel"></param>
        /// <returns></returns>
        internal static bool IsSteel(List<string> checked_reports, List<string> elements_steel)
        {
            var steel = false;

            for (var i = 0; i < checked_reports.Count; i++)
            {
                var raport = Path.GetFileNameWithoutExtension(checked_reports[i]);
                for (var j = 0; j < elements_steel.Count; j++)
                {
                    if (Regex.Match(raport, elements_steel[j]).Success)
                        steel = true;
                }
            }

            return steel;
        }

        /// <summary>
        /// Get output row from DataTable (converted by DataSource)
        /// </summary>
        /// <param name="tempDataSource"></param>
        /// <returns></returns>
        internal static int GetCountBy(object tempDataSource)
        {
            var result = 0;
            if (tempDataSource != null)
            {
                var _output = (DataTable)tempDataSource;
                result = _output.Rows.Count;
            }

            return result;
        }

        /// <summary>
        /// Loading logo from file
        /// </summary>
        /// <returns></returns>
        internal static string LoadLogo()
        {
            DirectoryInfo output = new DirectoryInfo(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
            FileInfo[] fileArray = output.GetFiles("logo.*", SearchOption.TopDirectoryOnly);

            return (fileArray.Length > 0) ? fileArray[0].FullName : "null";
        }

        /// <summary>
        /// Get logo from string64
        /// </summary>
        internal static void PutLogo(PictureBox tempPicture, TabPage tempTabPage)
        {
            var pathLogo = LoadLogo();// Base64ToImage(_image);

            if (pathLogo.Equals("null"))
                return;

            var tempLogo = Image.FromFile(pathLogo);
            tempPicture.Image = tempLogo;
            tempPicture.Location = new Point(tempTabPage.Size.Width - tempLogo.Width - tempTabPage.Margin.Right, tempTabPage.Size.Height - tempLogo.Height - tempTabPage.Margin.Bottom);
        }

        /// <summary>
        /// Remove white spaces
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        internal static string RemoveWhitespaces(string input)
        {
            var sb = new StringBuilder();
            var parts = input.Split(new char[] { ' ', '\n', '\t', '\r', '\f', '\v' },
                                          StringSplitOptions.RemoveEmptyEntries);
            foreach (var part in parts)
                sb.AppendFormat("{0}", part);
            return sb.ToString();
        }

        /// <summary>
        /// Reset source in datagridViews
        /// </summary>
        /// <param name="grids"></param>
        internal static void ResetDataGrids(Dictionary<string, MyGrids> grids)
        {
            for (var i = 0; i < grids.Count; i++)
            {
                var tempGrid = grids.ElementAt(i);
                tempGrid.Value.Grid.DataSource = null;
            }
        }

        /// <summary>
        /// Get language from selected template
        /// </summary>
        /// <param name="selected_template"></param>
        internal static void GetLanguage(string selected_template)
        {
            var count = 0;
            var tempLanguages = new string[] { "PL", "ENG", "DE" };
            for (var i = 0; i < tempLanguages.Length; i++)
            {
                var check_language = new Regex(tempLanguages[i]).Match(selected_template);
                if (check_language.Success)
                {
                    MyGlobal.Language = tempLanguages[i];
                    count++;
                }
            }

            if (count == 0)
                MyGlobal.Language = "PL";
        }

        /// <summary>
        /// Get reports from another environments 
        /// </summary>
        /// <param name="_attributes"></param>
        /// <param name="reports"></param>
        internal static void GetReports(string[] _attributes, out List<string> reports)
        {
            reports = new List<string>();

            for(var i = 0; i < _attributes.Length; i++)
            {
                var temp = string.Empty;
                TS.TeklaStructuresSettings.GetAdvancedOption(_attributes[i], ref temp);

                var pathes = temp.Split(';');
                if (pathes.Length > 1)
                {
                    for (var j = 0; j < pathes.Length; j++)
                        ReportsFromPath(pathes[j], ref reports);
                }
                else
                    ReportsFromPath(temp, ref reports);
            }
        }

        /// <summary>
        /// Read reports from path
        /// </summary>
        /// <param name="_path"></param>
        /// <param name="reports"></param>
        internal static void ReportsFromPath(string _path, ref List<string> reports)
        {
            if (!Directory.Exists(_path))
                return;

            var _getReports = Directory.GetFiles(_path, $"{MyGlobal.TitleReports}*.rpt", SearchOption.TopDirectoryOnly);
            if (_getReports.Length == 0)
                return;

            var _nameReports = _getReports.Select(x => Path.GetFileName(x)).ToList();
            for(var i = 0; i < _nameReports.Count; i++)
            {
                var _selectedNameReports = _nameReports[i];
                if (!reports.Contains(_selectedNameReports))
                    reports.Add(_selectedNameReports);
            }
        }

        /// <summary>
        /// Set data to document
        /// </summary>
        /// <param name="controls"></param>
        /// <param name="new_document"></param>
        internal static void SetDocument(Dictionary<string, Control> controls, Document new_document)
        {
            var _date = controls["date"] as DateTimePicker;

            controls["project"].Text = new_document.NumerProjektu;
            controls["document"].Text = new_document.NumerDokumentu;
            controls["rev"].Text = new_document.Rewizja;
            controls["phase"].Text = new_document.Faza;
            controls["object"].Text = new_document.Obiekt;
            controls["createdby"].Text = new_document.Projektant;
            controls["checkedby"].Text = new_document.Konstruktor;
            controls["contractor"].Text = new_document.Zamawiajacy;
            controls["name"].Text = new_document.Nazwa;
            _date.Value = new_document.Data;
            controls["document_other"].Text = new_document.NumerDokumentuObcy;
            controls["title"].Text = new_document.Tytul;
            controls["subtitle"].Text = new_document.Podtytul;
        }

        /// <summary>
        /// Clear controls like textbox, datetimepicker, combobox
        /// </summary>
        /// <param name="controls"></param>
        internal static void ClearControls(Dictionary<string, Control> controls)
        {
            foreach(var temp in controls.Values)
            {
                if (temp is TextBox _text)
                {
                    _text.Text = "";
                }
                else if (temp is DateTimePicker _datetime)
                {
                    _datetime.Value = DateTime.Now;
                }
                else if (temp is ComboBox _combo)
                {
                    _combo.SelectedIndex = 0;
                }
            }
        }

        /// <summary>
        /// Get data from document
        /// </summary>
        /// <param name="controls"></param>
        /// <param name="new_document"></param>
        internal static void GetDocument(Dictionary<string, Control> controls, out Document new_document)
        {
            var _date = controls["date"] as DateTimePicker;
            new_document = new Document
            {
                NumerProjektu = controls["project"].Text,
                NumerDokumentu = controls["document"].Text,
                Rewizja = controls["rev"].Text,
                Faza = controls["phase"].Text,
                Obiekt = controls["object"].Text,
                Projektant = controls["createdby"].Text,
                Konstruktor = controls["checkedby"].Text,
                Zamawiajacy = controls["contractor"].Text,
                Nazwa = controls["name"].Text,
                Data = _date.Value,
                NumerDokumentuObcy = controls["document_other"].Text,
                Tytul = controls["title"].Text,
                Podtytul = controls["subtitle"].Text
            };
        }

        /// <summary>
        /// Get attribute from file xml to check
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        internal static string CheckFile(string file)
        {
            var name = "";
            using (XmlReader reader = XmlReader.Create(file))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "Plik":
                                if (reader.Read())
                                    name = reader.Value.Trim();
                                break;
                        }
                    }
                }
            }

            return name;
        }

        /// <summary>
        /// Load reports file from Tekla / path
        /// </summary>
        /// <param name="checked_reports"></param>
        /// <param name="grids"></param>
        /// <param name="_progress"></param>
        internal static void LoadReports(List<string> checked_reports, Dictionary<string, MyGrids>  grids)
        {
            if (checked_reports.Count == 0)
            {
                MessageBox.Show("There are no outputs with report");
                return;
            }

            var elements_steel = GenerateElements();

            if (IsSteel(checked_reports, elements_steel))
            {
                for (var i = 0; i < checked_reports.Count; i++)
                {
                    var temp_file = Path.GetFileNameWithoutExtension(checked_reports[i]);

                    var tempText = "VE";
                    if (IsMatched(temp_file, tempText))
                    { 
                        MyBOM.VE(checked_reports[i], grids[tempText].Grid);
                        MyGlobal.FolderOutputs = Path.GetDirectoryName(checked_reports[i]);
                    }

                    tempText = "ST";
                    if (IsMatched(temp_file, tempText))
                    {
                        MyBOM.ST(checked_reports[i], grids[tempText].Grid);
                        MyGlobal.FolderOutputs = Path.GetDirectoryName(checked_reports[i]);
                    }

                    tempText = "M";
                    if (IsMatched(temp_file, tempText))
                    {
                        MyBOM.M(checked_reports[i], grids[tempText].Grid);
                        MyGlobal.FolderOutputs = Path.GetDirectoryName(checked_reports[i]);
                    }

                    tempText = "SMS_M";
                    if (IsMatched(temp_file, tempText))
                    {
                        MyBOM.SMSM(checked_reports[i], grids[tempText].Grid);
                        MyGlobal.FolderOutputs = Path.GetDirectoryName(checked_reports[i]);
                    }

                    tempText = "SMS_W";
                    if (IsMatched(temp_file, tempText))
                    {
                        MyBOM.SMSW(checked_reports[i], grids[tempText].Grid);
                        MyGlobal.FolderOutputs = Path.GetDirectoryName(checked_reports[i]);
                    }

                    tempText = "Kotwy";
                    if (IsMatched(temp_file, tempText))
                    {
                        MyBOM.A(checked_reports[i], grids[tempText].Grid);
                        MyGlobal.FolderOutputs = Path.GetDirectoryName(checked_reports[i]);
                    }

                    tempText = "Single";
                    if (IsMatched(temp_file, tempText))
                    {
                        MyBOM.S(checked_reports[i], grids[tempText].Grid);
                        MyGlobal.FolderOutputs = Path.GetDirectoryName(checked_reports[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Read all revisions from file text
        /// </summary>
        /// <param name="_revision"></param>
        internal static void GetRevisions(ComboBox _revision)
        {
            _revision.Items.Clear();

            var file_revision = Path.Combine(MyGlobal.AppPath, "revisions.txt");
            if (File.Exists(file_revision))
            {
                var revisions = File.ReadAllLines(file_revision, Encoding.UTF8);
                _revision.Items.AddRange(revisions.ToArray());
            }

            if (_revision.Items.Count > 0)
                _revision.SelectedIndex = 0;
        }

        /// <summary>
        /// Read all templates in directory from output
        /// </summary>
        /// <param name="c_environment_selected"></param>
        internal static void GetTemplates(ComboBox c_environment_selected, ComboBox c_template, string folder = "Steel")
        {
            if (c_environment_selected.SelectedIndex < 0)
                return;

            var selected_environment = c_environment_selected.Items[c_environment_selected.SelectedIndex].ToString();
            var templates_list = Directory.GetFiles(Path.Combine(MyGlobal.defFolderTemp, selected_environment, folder), "*.xlsx", SearchOption.TopDirectoryOnly);
            if (templates_list.Length > 0)
            {
                c_template.Items.Clear();
                for (var i = 0; i < templates_list.Length; i++)
                    c_template.Items.Add(Path.GetFileName(templates_list[i]));

                c_template.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Get environments with templates from app
        /// </summary>
        /// <param name="c_environment"></param>
        internal static void GetEnvironments(ComboBox c_environment)
        {
            var unallowed_directories = new List<string>() { "Kopia", "Copy", "Steel", "Stal", "Zelbet", "Concrete", "Obudowa", "Panel" };

            if (!Directory.Exists(MyGlobal.defFolderTemp))
            {
                MessageBox.Show("Templates not found in folder!");
                return;
            }

            var directories = Directory.GetDirectories(MyGlobal.defFolderTemp);
            for (var i = 0; i < directories.Length; i++)
            {
                var temp_folder = new DirectoryInfo(directories[i]);
                if (unallowed_directories.Contains(temp_folder.Name)) continue;

                c_environment.Items.Add(temp_folder.Name);
            }

            if (c_environment.Items.Count > 0)
                c_environment.SelectedIndex = 0;
        }
    }
}
