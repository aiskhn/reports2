﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Xml;

namespace Reports2
{
    internal class MyLists
    {
        #region VE
        /// <summary>
        /// Reading file XML for VE
        /// </summary>
        /// <param name="file"></param>
        /// <param name="assm"></param>
        /// <param name="assm_added"></param>
        /// <param name="count_input_assm"></param>
        /// <param name="data"></param>
        internal static void ReadVE(string file, out List<string> assm, out List<string> assm_added, out int count_input_assm, out List<string> data)
        {
            assm = new List<string>();
            assm_added = new List<string>();
            data = new List<string>();
            var collect_assm = new List<string>();

            using (XmlReader reader = XmlReader.Create(file))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "Numer":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "Ilosc":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "Gabaryt":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(MyTools.RemoveWhitespaces(reader.Value.Trim()));
                                break;
                            case "Length":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(MyTools.RemoveWhitespaces(reader.Value.Trim()));
                                break;
                            case "Waga":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "WagaCalk":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "Pow":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "Profil":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "temp1":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "temp2":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "temp3":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "temp4":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "temp5":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "temp6":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "temp7":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "Faza":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                            case "WagaSuma":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                            case "PowSuma":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                        }
                    }
                }
            }

            count_input_assm = collect_assm.Count;
        }

        /// <summary>
        /// Creating table for VE
        /// </summary>
        /// <param name="assm"></param>
        /// <param name="count_input_assm"></param>
        /// <param name="assm_added"></param>
        /// <returns></returns>
        internal static DataTable TableVE(List<string> assm, int count_input_assm, List<string> assm_added)
        {
            var rows_assm = assm.Count / count_input_assm;

            var _table = new DataTable("VE");
            _table.Columns.Add("Pozycja");
            _table.Columns.Add("Szt.");
            _table.Columns.Add("Gabaryty");
            _table.Columns.Add("Długosc");
            _table.Columns.Add("Masa");
            _table.Columns.Add("Masa całk.");
            _table.Columns.Add("Pow. mal.");
            _table.Columns.Add("Profil prowadzący");
            _table.Columns.Add("wykonanie");
            _table.Columns.Add("laczniki");
            _table.Columns.Add("wykonczenie");
            _table.Columns.Add("przeciwposlizgowe");
            _table.Columns.Add("antykorozyjne");
            _table.Columns.Add("kolnierz");
            _table.Columns.Add("gratingCount");

            for (var i = 0; i < rows_assm; i++)
            {
                _table.Rows.Add(new string[] {
                    assm[(i * count_input_assm) + 0],
                    assm[(i * count_input_assm) + 1],
                    "",
                    assm[(i * count_input_assm) + 3],
                    assm[(i * count_input_assm) + 4],
                    assm[(i * count_input_assm) + 5],
                    assm[(i * count_input_assm) + 6],
                    assm[(i * count_input_assm) + 7],
                    assm[(i * count_input_assm) + 8],
                    assm[(i * count_input_assm) + 9],
                    assm[(i * count_input_assm) + 10],
                    assm[(i * count_input_assm) + 11],
                    assm[(i * count_input_assm) + 12],
                    assm[(i * count_input_assm) + 13],
                    assm[(i * count_input_assm) + 14]
                });
                _table.Rows.Add(new string[]
                {
                    "",
                    "",
                    assm[(i * count_input_assm) + 2]
                });
            }

            return _table;
        }
        #endregion

        #region ST
        /// <summary>
        /// Reading file XML for ST
        /// </summary>
        /// <param name="file"></param>
        /// <param name="assm"></param>
        /// <param name="count_assm"></param>
        /// <param name="part"></param>
        /// <param name="count_part"></param>
        /// <param name="data"></param>
        internal static void ReadST(string file, out List<string> assm, out int count_assm, out List<string> part, out int count_part, out List<string> data)
        {
            var collect_assm = new List<string>();
            var collect_part = new List<string>();
            assm = new List<string>();
            part = new List<string>();
            data = new List<string>();

            using (XmlReader reader = XmlReader.Create(file))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "Numer":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "Ilosc":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "Profil":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "Material":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "Dlugosc":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "Waga":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "WagaCalk":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "Pow":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "Uwagi":
                                if (!collect_assm.Contains(reader.Name))
                                    collect_assm.Add(reader.Name);
                                if (reader.Read())
                                    assm.Add(reader.Value.Trim());
                                break;
                            case "NumerP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "IloscP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "ProfilP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "MaterialP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "DlugoscP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "WagaP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "WagaCalkP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "PowP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "UwagiP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "Ass_Pos":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "Faza":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                            case "WagaSuma":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                            case "PowSuma":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                        }
                    }
                }
            }

            count_assm = collect_assm.Count;
            count_part = collect_part.Count;
        }

        /// <summary>
        /// Creating table for ST
        /// </summary>
        /// <param name="assm"></param>
        /// <param name="count_input_assm"></param>
        /// <returns></returns>
        internal static DataTable TableST(List<string> assm, int count_input_assm)
        {
            var _table = new DataTable("ST");
            _table.Columns.Add("Pozycja");
            _table.Columns.Add("Szt.");
            _table.Columns.Add("Profil");
            _table.Columns.Add("Materiał");
            _table.Columns.Add("Długość");
            _table.Columns.Add("Masa");
            _table.Columns.Add("Masa całk.");
            _table.Columns.Add("Pow. mal.");
            _table.Columns.Add("Uwagi");
            _table.Columns.Add("Ass_Pos");

            var rows_assm = assm.Count / count_input_assm;

            for (var i = 0; i < rows_assm; i++)
            {
                _table.Rows.Add(new string[] {
                    assm[(i * count_input_assm) + 0],
                    assm[(i * count_input_assm) + 1],
                    assm[(i * count_input_assm) + 2],
                    assm[(i * count_input_assm) + 3],
                    assm[(i * count_input_assm) + 4],
                    assm[(i * count_input_assm) + 5],
                    assm[(i * count_input_assm) + 6],
                    assm[(i * count_input_assm) + 7],
                    assm[(i * count_input_assm) + 8],
                    "T"
                });
            }

            return _table;
        }

        /// <summary>
        /// Creating second table for ST
        /// </summary>
        /// <param name="part"></param>
        /// <param name="count_input_part"></param>
        /// <returns></returns>
        internal static DataTable SecondTableST(List<string> part, int count_input_part)
        {
            var _table = new DataTable("ST_2");
            _table.Columns.Add("Pozycja");
            _table.Columns.Add("Szt.");
            _table.Columns.Add("Profil");
            _table.Columns.Add("Materiał");
            _table.Columns.Add("Długość");
            _table.Columns.Add("Masa");
            _table.Columns.Add("Masa całk.");
            _table.Columns.Add("Pow. mal.");
            _table.Columns.Add("Uwagi");
            _table.Columns.Add("Ass_Pos");

            var rows_part = part.Count / count_input_part;

            for (var i = 0; i < rows_part; i++)
            {
                _table.Rows.Add(new string[] {
                    part[(i * count_input_part) + 0],
                    part[(i * count_input_part) + 1],
                    part[(i * count_input_part) + 2],
                    part[(i * count_input_part) + 3],
                    part[(i * count_input_part) + 4],
                    part[(i * count_input_part) + 5],
                    part[(i * count_input_part) + 6],
                    part[(i * count_input_part) + 7],
                    part[(i * count_input_part) + 8],
                    part[(i * count_input_part) + 9]
                });
            }

            return _table;
        }

        /// <summary>
        /// Merging main with second table for ST
        /// </summary>
        /// <param name="_table"></param>
        /// <param name="_table2"></param>
        /// <returns></returns>
        internal static DataTable MergeST(DataTable _table, DataTable _table2)
        {
            var _result = _table.Clone();
            for (var i = 0; i < _table.Rows.Count; i++)
            {
                var dRow = _table.Rows[i];
                _result.Rows.Add(dRow.ItemArray);

                var position = (string)dRow["Pozycja"];
                var foundRow = _table2.Select("Ass_Pos = '" + position + "'");
                for (var j = 0; j < foundRow.Length; j++)
                {
                    var temp_row = foundRow[j];
                    temp_row[9] = "";
                    _result.Rows.Add(temp_row.ItemArray);
                }
            }

            return _result;
        }
        #endregion

        #region M
        /// <summary>
        /// Reading file XML for M
        /// </summary>
        /// <param name="file"></param>
        /// <param name="part"></param>
        /// <param name="count_part"></param>
        /// <param name="data"></param>
        internal static void ReadM(string file, out List<string> part, out int count_part, out List<string> data)
        {
            var collect_part = new List<string>();
            part = new List<string>();
            data = new List<string>();

            using (XmlReader reader = XmlReader.Create(file))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "Profil":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "Pozycja":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "Material":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "Ilosc":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "Dlugosc":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "Powierzchnia":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "Waga":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "Uwagi":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "DlugoscSuma":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "PowSuma":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "WagaSuma":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "Faza":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                            case "PowTotal":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                            case "WagaTotal":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                            case "LengthTotal":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                        }
                    }
                }
            }

            count_part = collect_part.Count;
        }

        /// <summary>
        /// Creating table for M
        /// </summary>
        /// <param name="part"></param>
        /// <param name="count_input_part"></param>
        /// <returns></returns>
        internal static DataTable TableM(List<string> part, int count_input_part)
        {
            var _table = new DataTable("M");
            _table.Columns.Add("Profil");
            _table.Columns.Add("Pozycja");
            _table.Columns.Add("Material");
            _table.Columns.Add("Ilosc");
            _table.Columns.Add("Dlugosc");
            _table.Columns.Add("Powierzchnia");
            _table.Columns.Add("Waga");
            _table.Columns.Add("Uwagi");
            _table.Columns.Add("DlugoscSuma");
            _table.Columns.Add("PowSuma");
            _table.Columns.Add("WagaSuma");

            var rows_part = part.Count / count_input_part;

            for (var i = 0; i < rows_part; i++)
                _table.Rows.Add(new object[] {
                    part[(i * count_input_part) + 0],
                    part[(i * count_input_part) + 1],
                    part[(i * count_input_part) + 2],
                    part[(i * count_input_part) + 3],
                    part[(i * count_input_part) + 4],
                    part[(i * count_input_part) + 5],
                    part[(i * count_input_part) + 6],
                    part[(i * count_input_part) + 7],
                    part[(i * count_input_part) + 8],
                    part[(i * count_input_part) + 9],
                    part[(i * count_input_part) + 10]
                });

            return _table;
        }

        /// <summary>
        /// Merging materials for M
        /// </summary>
        /// <param name="_table"></param>
        /// <param name="profiles"></param>
        /// <returns></returns>
        internal static DataTable MergeM(DataTable _table, List<string> profiles)
        {
            var _new_table = new DataTable();
            _new_table.Columns.Add("Pozycja");
            _new_table.Columns.Add("Profil");
            _new_table.Columns.Add("Ilosc");
            _new_table.Columns.Add("Material");
            _new_table.Columns.Add("Dlugosc");
            _new_table.Columns.Add("Waga");
            _new_table.Columns.Add("Waga_całk");
            _new_table.Columns.Add("Powierzchnia");
            _new_table.Columns.Add("Uwagi");

            for (var i = 0; i < profiles.Count; i++)
            {
                var temp_profile = profiles[i];
                var foundRows = _table.Select("Profil = '" + temp_profile + "'");

                _new_table.Rows.Add(new object[] { "P" , temp_profile });

                var count = foundRows.Length;
                if (count == 1)
                    AddToTable(foundRows, ref _new_table);
                else if (count > 1)
                    SummaryAndAddToTable(foundRows, _new_table);
            }

            if (MyGlobal.L_M == "Uzupełnij" || MyGlobal.L_M == "0")
            {
                var totalLength = 0.0;
                for (int i = 0; i < _new_table.Rows.Count; i++)
                {
                    var tempRow = _new_table.Rows[i];
                    var firstRow = tempRow[0];
                    if (firstRow.ToString() == "SUMA")
                    {
                        var tempLength = tempRow["Dlugosc"];
                        if (tempLength != null)
                        {
                            var dlug_temp = double.Parse(tempLength.ToString(), CultureInfo.InvariantCulture);
                            if (dlug_temp > 0.0)
                                totalLength += dlug_temp;
                        }
                    }
                }

                var length_summ = totalLength.ToString().Replace(",", ".");
                
                MyGlobal.L_M = totalLength > 0.0 ? length_summ : "Uzupełnij";
            }

            return _new_table;
        }

        /// <summary>
        /// Summary for material
        /// </summary>
        /// <param name="foundRows"></param>
        /// <param name="_new_table"></param>
        private static void SummaryAndAddToTable(DataRow[] foundRows, DataTable _new_table)
        {
            var length_summary = new double();
            var area_summary = new double();
            var w_summary = new double();
            var length_summ = "";
            var area_summ = "";
            var weight_summ = "";

            for (var i = 0; i < foundRows.Length; i++)
            {
                var foundRow = foundRows[i];
                var profile = foundRow["Profil"].ToString();
                var position = foundRow["Pozycja"].ToString();
                var material = foundRow["Material"].ToString();
                var number = foundRow["Ilosc"].ToString();
                var length = foundRow["Dlugosc"].ToString();
                var area = foundRow["Powierzchnia"].ToString();
                var weight = foundRow["Waga"].ToString();
                var notice = foundRow["Uwagi"].ToString();
                length_summ = foundRow["DlugoscSuma"].ToString();
                area_summ = foundRow["PowSuma"].ToString();
                weight_summ = foundRow["WagaSuma"].ToString();

                _new_table.Rows.Add(new object[] { position, number, profile, material, length, weight, weight_summ, area, notice });

                var dlug_temp = double.Parse(length_summ, CultureInfo.InvariantCulture);
                var pow_temp = double.Parse(area_summ, CultureInfo.InvariantCulture);
                var w_temp = double.Parse(weight_summ, CultureInfo.InvariantCulture);
                length_summary += dlug_temp;
                area_summary += pow_temp;
                w_summary += w_temp;
            }

            length_summ = length_summary.ToString().Replace(",", ".");
            area_summ = area_summary.ToString().Replace(",", ".");
            weight_summ = w_summary.ToString().Replace(",", ".");

            _new_table.Rows.Add(new object[] { "SUMA", null, null, null, length_summ, null, weight_summ, area_summ, null });
            _new_table.Rows.Add(new object[] { "NEWLINE" });
        }

        /// <summary>
        /// Adding material data to table
        /// </summary>
        /// <param name="foundRows"></param>
        /// <param name="_new_table"></param>
        private static void AddToTable(DataRow[] foundRows, ref DataTable _new_table)
        {
            var row = foundRows[0];
            var profile = row["Profil"].ToString();
            var position = row["Pozycja"].ToString();
            var material = row["Material"].ToString();
            var number = row["Ilosc"].ToString();
            var length = row["Dlugosc"].ToString();
            var area = row["Powierzchnia"].ToString();
            var weight = row["Waga"].ToString();
            var notice = row["Uwagi"].ToString();
            var length_summ = row["DlugoscSuma"].ToString();
            var area_summ = row["PowSuma"].ToString();
            var weight_summ = row["WagaSuma"].ToString();

            _new_table.Rows.Add(new object[] { position, number, profile, material, length, weight, weight_summ, area, notice });
            _new_table.Rows.Add(new object[] { "SUMA", null, null, null, length_summ, null, weight_summ, area_summ, null });
            _new_table.Rows.Add(new object[] { "NEWLINE" });
        }

        /// <summary>
        /// Collecting profiles with no duplicates
        /// </summary>
        /// <param name="_table"></param>
        /// <returns></returns>
        internal static List<string> CollectProfiles(DataTable _table)
        {
            var _profiles = new List<string>();

            for (var i = 0; i < _table.Rows.Count; i++)
            {
                var temp = _table.Rows[i]["Profil"].ToString();
                if (!_profiles.Contains(temp))
                    _profiles.Add(temp);
            }

            return _profiles;
        }
        #endregion

        #region SMS_M & SMS_W
        /// <summary>
        /// Reading file XML for B
        /// </summary>
        /// <param name="file"></param>
        /// <param name="bolt"></param>
        /// <param name="bolt_list"></param>
        /// <param name="count_bolt"></param>
        /// <param name="data"></param>
        internal static void ReadB(string file, out List<string> bolt, out List<string> bolt_list, out int count_bolt, out List<string> data)
        {
            bolt = new List<string>();
            bolt_list = new List<string>();
            data = new List<string>();

            using (XmlReader reader = XmlReader.Create(file))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "B1":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "B2":
                                if (reader.Read())
                                {
                                    bolt.Add(reader.Value.Trim());
                                    bolt_list.Add(reader.Value.Trim());
                                }
                                break;
                            case "B3":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "B4":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "B5":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "B6":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "B7":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "B8":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "N1":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "N2":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "N3":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "N4":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "N5":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "N6":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "N7":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "N8":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "W1":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "W2":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "W3":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "W4":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "W5":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "W6":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "W7":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "W8":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "Faza":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                            case "WagaSuma":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                        }
                    }
                }
            }

            count_bolt = bolt.Count;
        }

        /// <summary>
        /// Creating table for B
        /// </summary>
        /// <param name="bolt"></param>
        /// <param name="bolt_list"></param>
        /// <returns></returns>
        internal static DataTable TableB(List<string> bolt, List<string> bolt_list)
        {
            var table = new DataTable();
            table.Columns.Add("Pozycja");
            table.Columns.Add("Ilosc");
            table.Columns.Add("Oznaczenie");
            table.Columns.Add("Norma");
            table.Columns.Add("Klasa");
            table.Columns.Add("Masa");
            table.Columns.Add("Masa całk.");
            table.Columns.Add("Opis");
            table.Columns.Add("Assm");

            var number = 1;
            for (var i = 0; i < bolt.Count; i = i + 8)
            {
                var myRow = table.NewRow();
                myRow[0] = "";
                if (bolt[i + 7] == "T")
                {
                    myRow[0] = number.ToString();
                    number++;
                }

                myRow[1] = bolt[i];
                myRow[2] = bolt[i + 1];
                myRow[3] = bolt[i + 2];
                myRow[4] = bolt[i + 3];
                myRow[5] = bolt[i + 4];
                myRow[6] = bolt[i + 5];
                myRow[7] = bolt[i + 6];
                myRow[8] = bolt[i + 7];
                table.Rows.Add(myRow);
            }

            var distinct = bolt_list.Distinct().ToList();
            var tableCollection = new DataSet("AssemblyBolt");

            for (var i = 0; i < distinct.Count; i++)
            {
                var temp_bolt = distinct[i];
                var foundRows = table.Select("Oznaczenie = '" + temp_bolt + "'");

                var ilosc_powtorzen = foundRows.Length;
                if (ilosc_powtorzen == 1)
                    CopyToTable(temp_bolt, tableCollection, table);
                else if (ilosc_powtorzen > 1)
                    SumAllRowsToOne(temp_bolt, tableCollection, table);
            }
            table = GroupsToTable(tableCollection, table);

            number = 1;
            for (var i = 0; i < table.Rows.Count; i++)
            {
                var assembly_main = (string)table.Rows[i][8];
                if (assembly_main == "T")
                {
                    table.Rows[i][0] = number.ToString();
                    number++;
                }
            }

            return table;
        }

        /// <summary>
        /// Grouping bolts and add to table
        /// </summary>
        /// <param name="tableCollection"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        private static DataTable GroupsToTable(DataSet tableCollection, DataTable table)
        {
            var new_table = new DataTable();
            new_table = table.Clone();

            var dtc = tableCollection.Tables;
            for (var i = 0; i < dtc.Count; i++)
            {
                var temp_table = dtc[i];
                for (var j = 0; j < temp_table.Rows.Count; j++)
                    new_table.ImportRow(temp_table.Rows[j]);
            }
            return new_table;
        }

        /// <summary>
        /// Summaring bolts
        /// </summary>
        /// <param name="temp_bolt"></param>
        /// <param name="tableCollection"></param>
        /// <param name="table"></param>
        private static void SumAllRowsToOne(string temp_bolt, DataSet tableCollection, DataTable table)
        {
            var new_table = new DataTable();
            new_table = table.Clone();
            new_table.TableName = "temporary_table";

            var pozycje_srub = new List<int>();
            var pozycje_srub_max = new List<int>();
            FindPosition(pozycje_srub, pozycje_srub_max, temp_bolt, table);

            var first = pozycje_srub[0];
            var liczba = pozycje_srub_max.Count - 1;
            var last = pozycje_srub_max[liczba];

            for (var i = first; i <= last; i++)
                new_table.ImportRow(table.Rows[i]);

            var names = new List<string>();
            var standards = new List<string>();
            var grades = new List<string>();

            for (var i = 0; i < new_table.Rows.Count; i++)
            {
                var name = new_table.Rows[i][2].ToString();
                var standard = new_table.Rows[i][3].ToString();
                var grade = new_table.Rows[i][4].ToString();

                if (name == temp_bolt)
                {
                    names.Add(name);
                    standards.Add(standard);
                    grades.Add(grade);
                }
            }

            names = names.Distinct().ToList();
            standards = standards.Distinct().ToList();
            grades = grades.Distinct().ToList();

            var temporary_table = new DataTable();
            var dataset = new DataSet("temp");

            for (var i = 0; i < names.Count; i++)
            {
                for (var j = 0; j < standards.Count; j++)
                {
                    for (var k = 0; k < grades.Count; k++)
                    {
                        var result = new_table.Select($"Oznaczenie = '{names[i]}' AND Norma = '{standards[j]}' AND Klasa = '{grades[k]}'");
                        if (result.Length > 0)
                        {
                            temporary_table = CreateSubGroup(result, new_table);
                            dataset.Tables.Add(temporary_table);
                        }
                    }
                }
            }

            var dtc = dataset.Tables;
            for (var i = 0; i < dtc.Count; i++)
            {
                var temp_table = dtc[i];
                var count1 = int.Parse(temp_table.Rows[0][1].ToString(), CultureInfo.InvariantCulture);
                var weight_total1 = float.Parse(temp_table.Rows[0][6].ToString(), CultureInfo.InvariantCulture);
                for (var j = 0; j < temp_table.Rows.Count; j++)
                {
                    var first_name = temp_table.Rows[0][2].ToString();
                    var next_name = temp_table.Rows[j][2].ToString();
                    var count2 = int.Parse(temp_table.Rows[j][1].ToString(), CultureInfo.InvariantCulture);
                    var weight_total2 = float.Parse(temp_table.Rows[j][6].ToString(), CultureInfo.InvariantCulture);
                    if (j > 0)
                    {
                        if (first_name == next_name)
                        {
                            count1 = count1 + count2;
                            weight_total1 = weight_total1 + weight_total2;
                            temp_table.Rows.RemoveAt(j);
                        }
                    }
                }

                temp_table.Rows[0][1] = count1;
                temp_table.Rows[0][6] = weight_total1.ToString().Replace(",", ".");

                for (var k = 1; k < temp_table.Rows.Count; k++)
                {
                    var ilosc = temp_table.Rows[k][1].ToString();
                    if (ilosc == "0")
                    {
                        temp_table.Rows.RemoveAt(k);
                        k = 1;
                    }
                }

                temp_table = SortTable(temp_table);

                tableCollection.Tables.Add(temp_table);
            }
        }

        /// <summary>
        /// Creating subgroup for bolts
        /// </summary>
        /// <param name="result"></param>
        /// <param name="new_table"></param>
        /// <returns></returns>
        private static DataTable CreateSubGroup(DataRow[] result, DataTable new_table)
        {
            var temp_table = new DataTable();
            temp_table = new_table.Clone();

            for (var i = 0; i < result.Length; i++)
            {
                if (i == 0)
                {
                    var full_name = $"{result[0][2]}_{result[0][3]}_{result[0][4]}";// result[0][2].ToString() + "_" + result[0][3].ToString() + "_" + result[0][4].ToString();
                    temp_table.TableName = full_name;
                }

                var found_position = result[i][0].ToString();
                for (var j = 0; j < new_table.Rows.Count; j++)
                {
                    var position = new_table.Rows[j][0].ToString();
                    if (found_position == position)
                    {
                        for (var k = 0; k < 6; k++)
                        {
                            var selected_row = new_table.Rows[j + k];
                            temp_table.ImportRow(selected_row);
                        }
                    }
                }
            }
            return temp_table;
        }

        /// <summary>
        /// Copy to table to not destroy original
        /// </summary>
        /// <param name="temp_bolt"></param>
        /// <param name="tableCollection"></param>
        /// <param name="table"></param>
        private static void CopyToTable(string temp_bolt, DataSet tableCollection, DataTable table)
        {
            var new_table = new DataTable();
            new_table = table.Clone();
            new_table.TableName = temp_bolt;

            var position_bolt = new List<int>();
            var position_bolt_max = new List<int>();
            FindPosition(position_bolt, position_bolt_max, temp_bolt, table);

            var first = position_bolt[0];
            var liczba = position_bolt_max.Count - 1;
            var last = position_bolt_max[liczba];

            for (var i = first; i <= last; i++)
                new_table.ImportRow(table.Rows[i]);

            var result = new_table.Select("Ilosc LIKE '0'");
            if (result.Length > 0)
            {
                for (var i = 0; i < result.Length; i++)
                    new_table.Rows.Remove(result[i]);
            }

            new_table = SortTable(new_table);

            tableCollection.Tables.Add(new_table);
        }

        /// <summary>
        /// Sorting by bolt standard and name
        /// </summary>
        /// <param name="new_table"></param>
        /// <returns></returns>
        private static DataTable SortTable(DataTable new_table)
        {
            var names = new List<string>();
            var standards = new List<string>();

            for (var i = 1; i < new_table.Rows.Count; i++)
            {
                var name = new_table.Rows[i][2].ToString();
                var standard = new_table.Rows[i][3].ToString();

                names.Add(name);
                standards.Add(standard);
            }

            names = names.Distinct().ToList();
            standards = standards.Distinct().ToList();

            var temp = new DataTable();
            temp = new_table.Clone();

            var first_row = new_table.Rows[0];
            temp.ImportRow(first_row);

            var count_result = 0;
            for (var i = 0; i < names.Count; i++)
            {
                for (var j = 0; j < standards.Count; j++)
                {
                    var name = names[i];
                    var standard = standards[j];
                    var result = new_table.Select("Oznaczenie = '" + name + "' AND Norma = '" + standard + "'");

                    var count = 0;
                    var weight_total = 0.0f;

                    for (var k = 0; k < result.Length; k++)
                    {
                        count_result++;
                        var temp_count = int.Parse(result[k][1].ToString(), CultureInfo.InvariantCulture);
                        var temp_weight_total = float.Parse(result[k][6].ToString(), CultureInfo.InvariantCulture);

                        count = count + temp_count;
                        weight_total = weight_total + temp_weight_total;

                        if (result.Length == count_result)
                        {
                            var new_row = temp.NewRow();
                            new_row[0] = "";
                            new_row[1] = count;
                            new_row[2] = name;
                            new_row[3] = standard;
                            new_row[4] = "";
                            new_row[5] = result[k][5];
                            new_row[6] = weight_total.ToString().Replace(",", ".");
                            new_row[7] = "";
                            new_row[8] = "";
                            temp.Rows.Add(new_row);
                        }
                    }
                    count_result = 0;
                }
            }
            return temp;
        }

        //Finding position bolt
        private static void FindPosition(List<int> position_bolt, List<int> position_bolt_max, string temp_bolt, DataTable table)
        {
            for (var i = 0; i < table.Rows.Count; i++)
            {
                var name_bolt = table.Rows[i][2].ToString();
                if (name_bolt == temp_bolt)
                {
                    position_bolt.Add(i);
                    var max_position = i + 5;
                    position_bolt_max.Add(max_position);
                }
            }
        }
        #endregion

        #region Kotwy
        /// <summary>
        /// Reading file XML for A (anchor)
        /// </summary>
        /// <param name="file"></param>
        /// <param name="bolt"></param>
        /// <param name="bolt_list"></param>
        /// <param name="count_bolt"></param>
        /// <param name="data"></param>
        internal static void ReadA(string file, out List<string> bolt, out List<string> bolt_list, out int count_bolt, out List<string> data)
        {
            bolt = new List<string>();
            bolt_list = new List<string>();
            data = new List<string>();

            using (XmlReader reader = XmlReader.Create(file))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "B1":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "B2":
                                if (reader.Read())
                                {
                                    bolt.Add(reader.Value.Trim());
                                    bolt_list.Add(reader.Value.Trim());
                                }
                                break;
                            case "B3":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "B4":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "B5":
                                if (reader.Read())
                                    bolt.Add(reader.Value.Trim());
                                break;
                            case "Faza":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                            case "IloscSuma":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                        }
                    }
                }
            }

            count_bolt = bolt.Count;
        }

        /// <summary>
        /// Creating table for A
        /// </summary>
        /// <param name="anchor"></param>
        /// <param name="anchor_list"></param>
        /// <returns></returns>
        internal static DataTable TableA(List<string> anchor, List<string> anchor_list)
        {
            var table = new DataTable();
            table.Columns.Add("Pozycja");
            table.Columns.Add("Ilość");
            table.Columns.Add("Oznaczenie");
            table.Columns.Add("Klasa");
            table.Columns.Add("Żywica");
            table.Columns.Add("Uwagi");

            var number = 1;
            for (var i = 0; i < anchor_list.Count; i++)
            {
                var tempI = i * 5;
                DataRow myRow = table.NewRow();
                myRow[0] = number.ToString();
                myRow[1] = anchor[tempI];
                myRow[2] = anchor[tempI + 1];
                myRow[3] = anchor[tempI + 2];
                myRow[4] = anchor[tempI + 3];
                myRow[5] = anchor[tempI + 4];
                table.Rows.Add(myRow);

                number++;
            }

            return table;
        }
        #endregion

        #region S
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="assm"></param>
        /// <param name="count_assm"></param>
        /// <param name="part"></param>
        /// <param name="count_part"></param>
        /// <param name="data"></param>
        internal static void ReadS(string file, out List<string> assm, out int count_assm, out List<string> part, out int count_part, out List<string> data)
        {
            var collect_assm = new List<string>();
            var collect_part = new List<string>();
            assm = new List<string>();
            part = new List<string>();
            data = new List<string>();

            using (XmlReader reader = XmlReader.Create(file))
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "NumerP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "IloscP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "ProfilP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "MaterialP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "Dlugosc":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "DlugoscP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "WagaP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "WagaCalkP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "PowP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "UwagiP":
                                if (!collect_part.Contains(reader.Name))
                                    collect_part.Add(reader.Name);
                                if (reader.Read())
                                    part.Add(reader.Value.Trim());
                                break;
                            case "Faza":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                            case "WagaSuma":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                            case "PowSuma":
                                if (reader.Read())
                                    data.Add(reader.Value.Trim());
                                break;
                        }
                    }
                }
            }

            count_assm = collect_assm.Count;
            count_part = collect_part.Count;
        }

        /// <summary>
        /// Creating table for S
        /// </summary>
        /// <param name="part"></param>
        /// <param name="count_input_part"></param>
        /// <returns></returns>
        internal static DataTable TableS(List<string> part, int count_input_part)
        {
            var _table = new DataTable("S");
            _table.Columns.Add("Pozycja");
            _table.Columns.Add("Szt.");
            _table.Columns.Add("Profil");
            _table.Columns.Add("Długość");
            _table.Columns.Add("Długość całk.");
            _table.Columns.Add("Masa");
            _table.Columns.Add("Masa całk.");
            _table.Columns.Add("Materiał");
            _table.Columns.Add("Pow. mal.");
            _table.Columns.Add("Uwagi");

            var rows_part = part.Count / count_input_part;

            for (var i = 0; i < rows_part; i++)
            {
                _table.Rows.Add(new string[] {
                    part[(i * count_input_part) + 0],
                    part[(i * count_input_part) + 1],
                    part[(i * count_input_part) + 2],
                    part[(i * count_input_part) + 4],
                    part[(i * count_input_part) + 5],
                    part[(i * count_input_part) + 6],
                    part[(i * count_input_part) + 7],
                    part[(i * count_input_part) + 3],
                    part[(i * count_input_part) + 8],
                    part[(i * count_input_part) + 9]
                });
            }

            return _table;
        }
        #endregion
    }
}