﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Reports2
{
    internal class MyGlobal
    {
        #region internal variables
        internal static string defComp = "RT";
        internal static string defLang = "PL";
        internal static string defTitleRep = "_RT_";
        internal static string defFolderRep = @"C:\";
        internal static string defFolderInp = @"C:\";
        internal static string defFolderOut = @"C:\";

        private static readonly string defAppPath = Path.GetDirectoryName(Application.ExecutablePath);

        internal static string defFolderTemp =
            Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Templates");

        internal static string defFolderDoc = $"{defComp}_Documents";

        internal static readonly string[] allowedLists = new string[]
            {"VE", "VE_", "ST", "ST_", "M", "M_", "SMS_M", "SMS_W", "Kotwy", "Single", "Single_"};

        internal static readonly string[] listRep = new string[] {"VE", "ST", "M", "SMS_M", "SMS_W", "Kotwy", "Single" };

        public static string Language
        {
            get { return defLang; }
            set { defLang = value; }
        }

        public static string FolderReports
        {
            get { return defFolderRep; }
            set { defFolderRep = value; }
        }

        public static string FolderOutputs
        {
            get { return defFolderOut; }
            set { defFolderOut = value; }
        }

        public static string FolderInputs
        {
            get { return defFolderInp; }
            set { defFolderInp = value; }
        }

        public static string AppPath
        {
            get { return defAppPath; }
        }

        public static string FolderTemplates
        {
            get { return defFolderTemp; }
            set { defFolderTemp = value; }
        }

        public static string FolderDocuments
        {
            get { return defFolderDoc; }
            set { defFolderDoc = value; }
        }

        public static string TitleReports
        {
            get { return defTitleRep; }
        }

        public static string W_VE { get; internal set; }
        public static string W_ST { get; internal set; }
        public static string A_ST { get; internal set; }

        internal static void SetFolderDocuments()
        {
            defFolderDoc = $"{defComp}_Documents";
        }

        public static string W_M { get; internal set; }
        public static string A_M { get; internal set; }
        public static string L_M { get; internal set; }
        public static string W_SMSM { get; internal set; }
        public static string W_SMSW { get; internal set; }
        public static string C_A { get; internal set; }
        public static string A_VE { get; internal set; }
        public static string wykon_VE { get; internal set; }
        public static string wykoncz_VE { get; internal set; }
        public static string antyposl_VE { get; internal set; }
        public static string uchwyt_VE { get; internal set; }
        public static string Length_M { get; internal set; }
        public static string Weight_B { get; internal set; }
        public static string W_S { get; internal set; }
        #endregion

        #region languages
        internal static readonly string fileTotal = Path.Combine(AppPath, "Languages/total.json");
        internal static readonly string fileSummary = Path.Combine(AppPath, "Languages/summary.json");
        internal static readonly string fileNut = Path.Combine(AppPath, "Languages/nut.json");
        internal static readonly string fileWasher = Path.Combine(AppPath, "Languages/washer.json");
        internal static readonly string fileGrating = Path.Combine(AppPath, "Languages/grating.json");

        internal static string gratingString =
            "{\"ENG\":{\"anchor1\":\"HILTI X-CR M8-15-12FP10 (stainless steel)\",\"anchor2\":\"HILTI X-EM8H-15-12P10 (galvanic zinced)\",\"anticorrosive\":\"Anticorrosive protection: HOT-DIP GALVANIZED\",\"clamp\":\"Double clamps:\",\"connector1\":\"Connectors:\",\"connector2\":\"HILTI X-FCM-M 25/30 (Hot galvanized)\",\"connector3\":\"HILTI X-FCM-M 35/40 (Hot galvanized)\",\"connector4\":\"HILTI X-FCM-M 45/50 (Hot galvanized)\",\"connector5\":\"HILTI X-FCM-R 25/30 (Stainless steel)\",\"connector6\":\"HILTI X-FCM-R 35/40 (Stainless steel)\",\"connector7\":\"HILTI X-FCM-R 45/50 (Stainless steel)\",\"connector8\":\"HILTI X-GR RU 25/30 CR20 P8 (Hot galvanized)\",\"connector9\":\"HILTI X-GR RU 35/40 CR20 P8 (Hot galvanized)\",\"finishing1\":\"Finishing: BORDERED\",\"finishing2\":\"Finishing: NOT BORDERED\",\"grating1\":\"REMARKS: GRATINGS\",\"grating2\":\"Nicks, kickplates ect. are shown on workshop domumentation drawings.\\nKickplates must be welded to gratings.\",\"material\":\"Material: S235JR or equivalent according to manufacturer indication\",\"production1\":\"Method of production: WELDED\",\"production2\":\"Method of production: PRESSED\",\"quantity\":\"pcs\",\"serrated1\":\"Serrated: NO (Exeptions describe \\\"Serrated\\\")\",\"serrated2\":\"Serrated: YES\",\"step1\":\"REMARKS: STEPS\",\"step2\":\"Non-skid angel: YES\"}}";

        internal static readonly Dictionary<string, string> langTotal =
            JsonConvert.DeserializeObject<Dictionary<string, string>>(File.Exists(fileTotal)
                ? File.ReadAllText(fileTotal)
                : "{'ENG':'Total'}, {'PL':'Razem'}");

        internal static readonly Dictionary<string, string> langSummary =
            JsonConvert.DeserializeObject<Dictionary<string, string>>(File.Exists(fileSummary)
                ? File.ReadAllText(fileSummary)
                : "{'ENG':'Summary'}, {'PL':'Suma'}");

        internal static readonly Dictionary<string, string> langNut =
            JsonConvert.DeserializeObject<Dictionary<string, string>>(File.Exists(fileNut)
                ? File.ReadAllText(fileNut)
                : "{'ENG':'Nut'}, {'PL':'Nakrętka'}");

        internal static readonly Dictionary<string, string> langWasher =
            JsonConvert.DeserializeObject<Dictionary<string, string>>(File.Exists(fileWasher)
                ? File.ReadAllText(fileWasher)
                : "{'ENG':'Washer'}, , {'PL':'Podkładka'}");

        internal static readonly Dictionary<string, MyGrating> langGrating =
            JsonConvert.DeserializeObject<Dictionary<string, MyGrating>>(File.Exists(fileGrating)
                ? File.ReadAllText(fileGrating)
                : gratingString);
        #endregion
    }
}
