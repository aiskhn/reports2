﻿using System.Collections;

using TSM = Tekla.Structures.Model;

namespace Reports2
{
    internal class TeklaStuff
    {
        /// <summary>
        /// Collect all attributes
        /// </summary>
        internal static readonly string[] allAttributes = new string[] { "XS_TEMPLATE_DIRECTORY", "XS_DRIVER", "XS_SYSTEM" };

        /// <summary>
        /// Get selected members from Tekla model
        /// </summary>
        /// <returns></returns>
        internal static ArrayList GetMembersFromModel()
        {
            var objects = new ArrayList();
            var mos = new TSM.UI.ModelObjectSelector();
            var moe = mos.GetSelectedObjects();
            while (moe.MoveNext())
            {
                var mo = moe.Current as TSM.ModelObject;
                objects.Add(mo);
            }

            var result = mos.Select(objects);

            return objects;
        }
    }
}