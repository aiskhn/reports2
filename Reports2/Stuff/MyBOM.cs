﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace Reports2
{
    internal class MyBOM
    {
        /// <summary>
        /// Transfer data from file XML to table and show in main form - S
        /// </summary>
        /// <param name="file"></param>
        /// <param name="gridControl"></param>
        internal static void S(string file, DataGridView gridControl)
        {
            if (MyTools.CheckFile(file) != "S")
                return;

            MyLists.ReadS(file, out List<string> assm, out int count_input_assm, out List<string> part, out int count_input_part, out List<string> data);

            if (part.Count == 0)
                return;

            if (data.Count > 0)
            {
                MyGlobal.W_S = data[1];
            }

            var _output = MyLists.TableS(part, count_input_part);

            if (_output.Rows.Count == 0)
                return;

            gridControl.DataSource = _output;
        }

        /// <summary>
        /// Transfer data from file XML to table and show in main form - A
        /// </summary>
        /// <param name="file"></param>
        /// <param name="gridControl"></param>
        internal static void A(string file, DataGridView gridControl)
        {
            if (MyTools.CheckFile(file) != "Kotwy")
                return;

            MyLists.ReadA(file, out List<string> bolt, out List<string> bolt_list, out int count_bolt, out List<string> data);

            if (count_bolt == 0)
                return;

            if (data.Count > 0)
                MyGlobal.C_A = data[1];

            var _output = MyLists.TableA(bolt, bolt_list);
            if (_output.Rows.Count == 0)
                return;

            gridControl.DataSource = _output;
        }

        /// <summary>
        /// Transfer data from file XML to table and show in main form - SMS_W
        /// </summary>
        /// <param name="file"></param>
        /// <param name="gridControl"></param>
        internal static void SMSW(string file, DataGridView gridControl)
        {
            var fileName = MyTools.CheckFile(file);

            if (fileName != "SMS_W")
                return;

            MyLists.ReadB(file, out List<string> bolt, out List<string> bolt_list, out int count_bolt, out List<string> data);

            if (count_bolt == 0)
                return;

            if (data.Count > 0)
                MyGlobal.W_SMSW = data[1];

            var _output = MyLists.TableB(bolt, bolt_list);
            if (_output.Rows.Count == 0)
                return;

            gridControl.DataSource = _output;
            gridControl.Columns[gridControl.ColumnCount - 1].Visible = false;
        }

        /// <summary>
        /// Transfer data from file XML to table and show in main form - SMS_M
        /// </summary>
        /// <param name="file"></param>
        /// <param name="gridControl"></param>
        internal static void SMSM(string file, DataGridView gridControl)
        {
            if (MyTools.CheckFile(file) != "SMS_M")
                return;

            MyLists.ReadB(file, out List<string> bolt, out List<string> bolt_list, out int count_bolt, out List<string> data);

            if (count_bolt == 0)
                return;

            if (data.Count > 0)
                MyGlobal.W_SMSM = data[1];

            var _output = MyLists.TableB(bolt, bolt_list);
            if (_output.Rows.Count == 0)
                return;

            gridControl.DataSource = _output;
            gridControl.Columns[gridControl.ColumnCount - 1].Visible = false;
        }

        /// <summary>
        /// Transfer data from file XML to table and show in main form - M
        /// </summary>
        /// <param name="file"></param>
        /// <param name="gridControl"></param>
        internal static void M(string file, DataGridView gridControl)
        {
            if (MyTools.CheckFile(file) != "M")
                return;

            MyLists.ReadM(file, out List<string> part, out int count_input_part, out List<string> data);

            if (part.Count == 0)
                return;

            if (data.Count > 0)
            {
                MyGlobal.L_M = data.Count > 3 ? data[3]: "Uzupełnij";
                MyGlobal.W_M = data[2];
                MyGlobal.A_M = data[1];
            }

            var _table = MyLists.TableM(part, count_input_part);

            var profiles = MyLists.CollectProfiles(_table);

            var _output = MyLists.MergeM(_table, profiles);
            if (_output.Rows.Count == 0)
                return;

            gridControl.DataSource = _output;
        }

        /// <summary>
        /// Transfer data from file XML to table and show in main form - ST
        /// </summary>
        /// <param name="file"></param>
        /// <param name="gridControl"></param>
        internal static void ST(string file, DataGridView gridControl)
        {
            if (MyTools.CheckFile(file) != "ST")
                return;

            MyLists.ReadST(file, out List<string> assm, out int count_input_assm, out List<string> part, out int count_input_part, out List<string> data);

            if (assm.Count == 0 || part.Count == 0)
                return;

            if (data.Count > 0)
            {
                MyGlobal.W_ST = data[1];
                MyGlobal.A_ST = data[2];
            }

            var _table = MyLists.TableST(assm, count_input_assm);
            var _table2 = MyLists.SecondTableST(part, count_input_part);

            var _output = MyLists.MergeST(_table, _table2);
            if (_output.Rows.Count == 0)
                return;

            gridControl.DataSource = _output;

            gridControl.Columns[gridControl.ColumnCount - 1].Visible = false;
        }

        /// <summary>
        /// Transfer data from file XML to table and show in main form - VE
        /// </summary>
        /// <param name="file"></param>
        /// <param name="gridControl"></param>
        internal static void VE(string file, DataGridView gridControl)
        {
            if (MyTools.CheckFile(file) != "VE")
                return;

            MyLists.ReadVE(file, out List<string> assm, out List<string> assm_added, out int count_input_assm, out List<string> data);

            if (assm.Count == 0)
                return;

            if (data.Count > 0)
            {
                MyGlobal.W_VE = data[1];
                MyGlobal.A_VE = data[2];
            }

            var _output = MyLists.TableVE(assm, count_input_assm, assm_added);

            if (_output.Rows.Count == 0)
                return;

            gridControl.DataSource = _output;

            for (int i = 0; i < 7; i++)
                gridControl.Columns[gridControl.Columns.Count - (i + 1)].Visible = false;
        }
    }
}
