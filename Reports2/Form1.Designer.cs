﻿namespace Reports2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bOpen = new MetroFramework.Controls.MetroButton();
            this.bExcel = new MetroFramework.Controls.MetroButton();
            this.c_template = new MetroFramework.Controls.MetroComboBox();
            this.c_environment = new MetroFramework.Controls.MetroComboBox();
            this._loadreports = new MetroFramework.Controls.MetroButton();
            this.t_document_other = new MetroFramework.Controls.MetroTextBox();
            this.t_subtitle = new MetroFramework.Controls.MetroTextBox();
            this.t_title = new MetroFramework.Controls.MetroTextBox();
            this.d_date = new MetroFramework.Controls.MetroDateTime();
            this.t_name = new MetroFramework.Controls.MetroTextBox();
            this.t_contractor = new MetroFramework.Controls.MetroTextBox();
            this.t_checkedby = new MetroFramework.Controls.MetroTextBox();
            this.t_createdby = new MetroFramework.Controls.MetroTextBox();
            this.t_object = new MetroFramework.Controls.MetroTextBox();
            this.b_rev = new MetroFramework.Controls.MetroButton();
            this.c_rev = new MetroFramework.Controls.MetroComboBox();
            this.t_phase = new MetroFramework.Controls.MetroTextBox();
            this.t_document = new MetroFramework.Controls.MetroTextBox();
            this.t_project = new MetroFramework.Controls.MetroTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bSave = new MetroFramework.Controls.MetroButton();
            this.bLoad = new MetroFramework.Controls.MetroButton();
            this.bClear = new MetroFramework.Controls.MetroButton();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.checkedLists = new System.Windows.Forms.CheckedListBox();
            this.bBOM = new MetroFramework.Controls.MetroButton();
            this._folder_output = new MetroFramework.Controls.MetroTextBox();
            this.tabCtrl = new MetroFramework.Controls.MetroTabControl();
            this.tabMain = new MetroFramework.Controls.MetroTabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabVE = new System.Windows.Forms.TabPage();
            this.gridVE = new System.Windows.Forms.DataGridView();
            this.tabST = new System.Windows.Forms.TabPage();
            this.gridST = new System.Windows.Forms.DataGridView();
            this.tabM = new System.Windows.Forms.TabPage();
            this.gridM = new System.Windows.Forms.DataGridView();
            this.tabS = new System.Windows.Forms.TabPage();
            this.gridS = new System.Windows.Forms.DataGridView();
            this.tabSMSM = new System.Windows.Forms.TabPage();
            this.gridSMSM = new System.Windows.Forms.DataGridView();
            this.tabSMSW = new System.Windows.Forms.TabPage();
            this.gridSMSW = new System.Windows.Forms.DataGridView();
            this.tabA = new System.Windows.Forms.TabPage();
            this.gridA = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabCtrl.SuspendLayout();
            this.tabMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabVE.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridVE)).BeginInit();
            this.tabST.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridST)).BeginInit();
            this.tabM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridM)).BeginInit();
            this.tabS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridS)).BeginInit();
            this.tabSMSM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSMSM)).BeginInit();
            this.tabSMSW.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSMSW)).BeginInit();
            this.tabA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridA)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bOpen);
            this.groupBox1.Controls.Add(this.bExcel);
            this.groupBox1.Controls.Add(this.c_template);
            this.groupBox1.Controls.Add(this.c_environment);
            this.groupBox1.Controls.Add(this._loadreports);
            this.groupBox1.Controls.Add(this.t_document_other);
            this.groupBox1.Controls.Add(this.t_subtitle);
            this.groupBox1.Controls.Add(this.t_title);
            this.groupBox1.Controls.Add(this.d_date);
            this.groupBox1.Controls.Add(this.t_name);
            this.groupBox1.Controls.Add(this.t_contractor);
            this.groupBox1.Controls.Add(this.t_checkedby);
            this.groupBox1.Controls.Add(this.t_createdby);
            this.groupBox1.Controls.Add(this.t_object);
            this.groupBox1.Controls.Add(this.b_rev);
            this.groupBox1.Controls.Add(this.c_rev);
            this.groupBox1.Controls.Add(this.t_phase);
            this.groupBox1.Controls.Add(this.t_document);
            this.groupBox1.Controls.Add(this.t_project);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(20, 60);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(250, 760);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dane";
            // 
            // bOpen
            // 
            this.bOpen.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.bOpen.Location = new System.Drawing.Point(6, 716);
            this.bOpen.Name = "bOpen";
            this.bOpen.Size = new System.Drawing.Size(116, 23);
            this.bOpen.TabIndex = 11;
            this.bOpen.Text = "Otwórz folder";
            this.bOpen.UseSelectable = true;
            this.bOpen.Click += new System.EventHandler(this.bOpen_Click);
            // 
            // bExcel
            // 
            this.bExcel.Location = new System.Drawing.Point(128, 716);
            this.bExcel.Name = "bExcel";
            this.bExcel.Size = new System.Drawing.Size(116, 23);
            this.bExcel.TabIndex = 12;
            this.bExcel.Text = "Generuj Excel";
            this.bExcel.UseSelectable = true;
            this.bExcel.Click += new System.EventHandler(this.bExcel_Click);
            // 
            // c_template
            // 
            this.c_template.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.c_template.FormattingEnabled = true;
            this.c_template.ItemHeight = 19;
            this.c_template.Location = new System.Drawing.Point(80, 670);
            this.c_template.Name = "c_template";
            this.c_template.Size = new System.Drawing.Size(164, 25);
            this.c_template.TabIndex = 49;
            this.c_template.UseSelectable = true;
            // 
            // c_environment
            // 
            this.c_environment.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.c_environment.FormattingEnabled = true;
            this.c_environment.ItemHeight = 19;
            this.c_environment.Location = new System.Drawing.Point(80, 632);
            this.c_environment.Name = "c_environment";
            this.c_environment.Size = new System.Drawing.Size(164, 25);
            this.c_environment.TabIndex = 48;
            this.c_environment.UseSelectable = true;
            this.c_environment.SelectedIndexChanged += new System.EventHandler(this.c_environment_SelectedIndexChanged);
            // 
            // _loadreports
            // 
            this._loadreports.Location = new System.Drawing.Point(6, 594);
            this._loadreports.Name = "_loadreports";
            this._loadreports.Size = new System.Drawing.Size(238, 23);
            this._loadreports.TabIndex = 9;
            this._loadreports.Text = "Wczytaj raporty";
            this._loadreports.UseSelectable = true;
            this._loadreports.Click += new System.EventHandler(this._loadreports_Click);
            // 
            // t_document_other
            // 
            // 
            // 
            // 
            this.t_document_other.CustomButton.Image = null;
            this.t_document_other.CustomButton.Location = new System.Drawing.Point(216, 1);
            this.t_document_other.CustomButton.Name = "";
            this.t_document_other.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.t_document_other.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.t_document_other.CustomButton.TabIndex = 1;
            this.t_document_other.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.t_document_other.CustomButton.UseSelectable = true;
            this.t_document_other.CustomButton.Visible = false;
            this.t_document_other.Lines = new string[0];
            this.t_document_other.Location = new System.Drawing.Point(6, 507);
            this.t_document_other.MaxLength = 32767;
            this.t_document_other.Name = "t_document_other";
            this.t_document_other.PasswordChar = '\0';
            this.t_document_other.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.t_document_other.SelectedText = "";
            this.t_document_other.SelectionLength = 0;
            this.t_document_other.SelectionStart = 0;
            this.t_document_other.ShortcutsEnabled = true;
            this.t_document_other.Size = new System.Drawing.Size(238, 23);
            this.t_document_other.TabIndex = 47;
            this.t_document_other.UseSelectable = true;
            this.t_document_other.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.t_document_other.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // t_subtitle
            // 
            // 
            // 
            // 
            this.t_subtitle.CustomButton.Image = null;
            this.t_subtitle.CustomButton.Location = new System.Drawing.Point(190, 2);
            this.t_subtitle.CustomButton.Name = "";
            this.t_subtitle.CustomButton.Size = new System.Drawing.Size(45, 45);
            this.t_subtitle.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.t_subtitle.CustomButton.TabIndex = 1;
            this.t_subtitle.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.t_subtitle.CustomButton.UseSelectable = true;
            this.t_subtitle.CustomButton.Visible = false;
            this.t_subtitle.Lines = new string[0];
            this.t_subtitle.Location = new System.Drawing.Point(6, 432);
            this.t_subtitle.MaxLength = 32767;
            this.t_subtitle.Multiline = true;
            this.t_subtitle.Name = "t_subtitle";
            this.t_subtitle.PasswordChar = '\0';
            this.t_subtitle.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.t_subtitle.SelectedText = "";
            this.t_subtitle.SelectionLength = 0;
            this.t_subtitle.SelectionStart = 0;
            this.t_subtitle.ShortcutsEnabled = true;
            this.t_subtitle.Size = new System.Drawing.Size(238, 50);
            this.t_subtitle.TabIndex = 46;
            this.t_subtitle.UseSelectable = true;
            this.t_subtitle.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.t_subtitle.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // t_title
            // 
            // 
            // 
            // 
            this.t_title.CustomButton.Image = null;
            this.t_title.CustomButton.Location = new System.Drawing.Point(168, 1);
            this.t_title.CustomButton.Name = "";
            this.t_title.CustomButton.Size = new System.Drawing.Size(69, 69);
            this.t_title.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.t_title.CustomButton.TabIndex = 1;
            this.t_title.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.t_title.CustomButton.UseSelectable = true;
            this.t_title.CustomButton.Visible = false;
            this.t_title.Lines = new string[0];
            this.t_title.Location = new System.Drawing.Point(6, 334);
            this.t_title.MaxLength = 32767;
            this.t_title.Multiline = true;
            this.t_title.Name = "t_title";
            this.t_title.PasswordChar = '\0';
            this.t_title.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.t_title.SelectedText = "";
            this.t_title.SelectionLength = 0;
            this.t_title.SelectionStart = 0;
            this.t_title.ShortcutsEnabled = true;
            this.t_title.Size = new System.Drawing.Size(238, 71);
            this.t_title.TabIndex = 8;
            this.t_title.UseSelectable = true;
            this.t_title.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.t_title.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // d_date
            // 
            this.d_date.FontSize = MetroFramework.MetroDateTimeSize.Small;
            this.d_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.d_date.Location = new System.Drawing.Point(115, 286);
            this.d_date.MinimumSize = new System.Drawing.Size(0, 25);
            this.d_date.Name = "d_date";
            this.d_date.Size = new System.Drawing.Size(129, 25);
            this.d_date.TabIndex = 8;
            // 
            // t_name
            // 
            // 
            // 
            // 
            this.t_name.CustomButton.Image = null;
            this.t_name.CustomButton.Location = new System.Drawing.Point(107, 1);
            this.t_name.CustomButton.Name = "";
            this.t_name.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.t_name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.t_name.CustomButton.TabIndex = 1;
            this.t_name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.t_name.CustomButton.UseSelectable = true;
            this.t_name.CustomButton.Visible = false;
            this.t_name.Lines = new string[0];
            this.t_name.Location = new System.Drawing.Point(115, 257);
            this.t_name.MaxLength = 32767;
            this.t_name.Name = "t_name";
            this.t_name.PasswordChar = '\0';
            this.t_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.t_name.SelectedText = "";
            this.t_name.SelectionLength = 0;
            this.t_name.SelectionStart = 0;
            this.t_name.ShortcutsEnabled = true;
            this.t_name.Size = new System.Drawing.Size(129, 23);
            this.t_name.TabIndex = 45;
            this.t_name.UseSelectable = true;
            this.t_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.t_name.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // t_contractor
            // 
            // 
            // 
            // 
            this.t_contractor.CustomButton.Image = null;
            this.t_contractor.CustomButton.Location = new System.Drawing.Point(107, 1);
            this.t_contractor.CustomButton.Name = "";
            this.t_contractor.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.t_contractor.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.t_contractor.CustomButton.TabIndex = 1;
            this.t_contractor.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.t_contractor.CustomButton.UseSelectable = true;
            this.t_contractor.CustomButton.Visible = false;
            this.t_contractor.Lines = new string[0];
            this.t_contractor.Location = new System.Drawing.Point(115, 228);
            this.t_contractor.MaxLength = 32767;
            this.t_contractor.Name = "t_contractor";
            this.t_contractor.PasswordChar = '\0';
            this.t_contractor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.t_contractor.SelectedText = "";
            this.t_contractor.SelectionLength = 0;
            this.t_contractor.SelectionStart = 0;
            this.t_contractor.ShortcutsEnabled = true;
            this.t_contractor.Size = new System.Drawing.Size(129, 23);
            this.t_contractor.TabIndex = 44;
            this.t_contractor.UseSelectable = true;
            this.t_contractor.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.t_contractor.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // t_checkedby
            // 
            // 
            // 
            // 
            this.t_checkedby.CustomButton.Image = null;
            this.t_checkedby.CustomButton.Location = new System.Drawing.Point(107, 1);
            this.t_checkedby.CustomButton.Name = "";
            this.t_checkedby.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.t_checkedby.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.t_checkedby.CustomButton.TabIndex = 1;
            this.t_checkedby.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.t_checkedby.CustomButton.UseSelectable = true;
            this.t_checkedby.CustomButton.Visible = false;
            this.t_checkedby.Lines = new string[0];
            this.t_checkedby.Location = new System.Drawing.Point(115, 199);
            this.t_checkedby.MaxLength = 32767;
            this.t_checkedby.Name = "t_checkedby";
            this.t_checkedby.PasswordChar = '\0';
            this.t_checkedby.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.t_checkedby.SelectedText = "";
            this.t_checkedby.SelectionLength = 0;
            this.t_checkedby.SelectionStart = 0;
            this.t_checkedby.ShortcutsEnabled = true;
            this.t_checkedby.Size = new System.Drawing.Size(129, 23);
            this.t_checkedby.TabIndex = 43;
            this.t_checkedby.UseSelectable = true;
            this.t_checkedby.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.t_checkedby.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // t_createdby
            // 
            // 
            // 
            // 
            this.t_createdby.CustomButton.Image = null;
            this.t_createdby.CustomButton.Location = new System.Drawing.Point(107, 1);
            this.t_createdby.CustomButton.Name = "";
            this.t_createdby.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.t_createdby.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.t_createdby.CustomButton.TabIndex = 1;
            this.t_createdby.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.t_createdby.CustomButton.UseSelectable = true;
            this.t_createdby.CustomButton.Visible = false;
            this.t_createdby.Lines = new string[0];
            this.t_createdby.Location = new System.Drawing.Point(115, 170);
            this.t_createdby.MaxLength = 32767;
            this.t_createdby.Name = "t_createdby";
            this.t_createdby.PasswordChar = '\0';
            this.t_createdby.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.t_createdby.SelectedText = "";
            this.t_createdby.SelectionLength = 0;
            this.t_createdby.SelectionStart = 0;
            this.t_createdby.ShortcutsEnabled = true;
            this.t_createdby.Size = new System.Drawing.Size(129, 23);
            this.t_createdby.TabIndex = 42;
            this.t_createdby.UseSelectable = true;
            this.t_createdby.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.t_createdby.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // t_object
            // 
            // 
            // 
            // 
            this.t_object.CustomButton.Image = null;
            this.t_object.CustomButton.Location = new System.Drawing.Point(107, 1);
            this.t_object.CustomButton.Name = "";
            this.t_object.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.t_object.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.t_object.CustomButton.TabIndex = 1;
            this.t_object.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.t_object.CustomButton.UseSelectable = true;
            this.t_object.CustomButton.Visible = false;
            this.t_object.Lines = new string[0];
            this.t_object.Location = new System.Drawing.Point(115, 141);
            this.t_object.MaxLength = 32767;
            this.t_object.Name = "t_object";
            this.t_object.PasswordChar = '\0';
            this.t_object.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.t_object.SelectedText = "";
            this.t_object.SelectionLength = 0;
            this.t_object.SelectionStart = 0;
            this.t_object.ShortcutsEnabled = true;
            this.t_object.Size = new System.Drawing.Size(129, 23);
            this.t_object.TabIndex = 41;
            this.t_object.UseSelectable = true;
            this.t_object.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.t_object.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // b_rev
            // 
            this.b_rev.BackgroundImage = global::Reports2.Properties.Resources.revisions;
            this.b_rev.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.b_rev.Location = new System.Drawing.Point(203, 106);
            this.b_rev.Name = "b_rev";
            this.b_rev.Size = new System.Drawing.Size(41, 29);
            this.b_rev.TabIndex = 8;
            this.b_rev.UseSelectable = true;
            this.b_rev.Click += new System.EventHandler(this.b_rev_Click);
            // 
            // c_rev
            // 
            this.c_rev.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.c_rev.FormattingEnabled = true;
            this.c_rev.ItemHeight = 19;
            this.c_rev.Location = new System.Drawing.Point(115, 106);
            this.c_rev.Name = "c_rev";
            this.c_rev.Size = new System.Drawing.Size(82, 25);
            this.c_rev.TabIndex = 9;
            this.c_rev.UseSelectable = true;
            // 
            // t_phase
            // 
            // 
            // 
            // 
            this.t_phase.CustomButton.Image = null;
            this.t_phase.CustomButton.Location = new System.Drawing.Point(107, 1);
            this.t_phase.CustomButton.Name = "";
            this.t_phase.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.t_phase.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.t_phase.CustomButton.TabIndex = 1;
            this.t_phase.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.t_phase.CustomButton.UseSelectable = true;
            this.t_phase.CustomButton.Visible = false;
            this.t_phase.Lines = new string[0];
            this.t_phase.Location = new System.Drawing.Point(115, 77);
            this.t_phase.MaxLength = 32767;
            this.t_phase.Name = "t_phase";
            this.t_phase.PasswordChar = '\0';
            this.t_phase.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.t_phase.SelectedText = "";
            this.t_phase.SelectionLength = 0;
            this.t_phase.SelectionStart = 0;
            this.t_phase.ShortcutsEnabled = true;
            this.t_phase.Size = new System.Drawing.Size(129, 23);
            this.t_phase.TabIndex = 40;
            this.t_phase.UseSelectable = true;
            this.t_phase.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.t_phase.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // t_document
            // 
            // 
            // 
            // 
            this.t_document.CustomButton.Image = null;
            this.t_document.CustomButton.Location = new System.Drawing.Point(107, 1);
            this.t_document.CustomButton.Name = "";
            this.t_document.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.t_document.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.t_document.CustomButton.TabIndex = 1;
            this.t_document.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.t_document.CustomButton.UseSelectable = true;
            this.t_document.CustomButton.Visible = false;
            this.t_document.Lines = new string[0];
            this.t_document.Location = new System.Drawing.Point(115, 48);
            this.t_document.MaxLength = 32767;
            this.t_document.Name = "t_document";
            this.t_document.PasswordChar = '\0';
            this.t_document.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.t_document.SelectedText = "";
            this.t_document.SelectionLength = 0;
            this.t_document.SelectionStart = 0;
            this.t_document.ShortcutsEnabled = true;
            this.t_document.Size = new System.Drawing.Size(129, 23);
            this.t_document.TabIndex = 39;
            this.t_document.UseSelectable = true;
            this.t_document.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.t_document.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // t_project
            // 
            // 
            // 
            // 
            this.t_project.CustomButton.Image = null;
            this.t_project.CustomButton.Location = new System.Drawing.Point(107, 1);
            this.t_project.CustomButton.Name = "";
            this.t_project.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.t_project.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.t_project.CustomButton.TabIndex = 1;
            this.t_project.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.t_project.CustomButton.UseSelectable = true;
            this.t_project.CustomButton.Visible = false;
            this.t_project.Lines = new string[0];
            this.t_project.Location = new System.Drawing.Point(115, 19);
            this.t_project.MaxLength = 32767;
            this.t_project.Name = "t_project";
            this.t_project.PasswordChar = '\0';
            this.t_project.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.t_project.SelectedText = "";
            this.t_project.SelectionLength = 0;
            this.t_project.SelectionStart = 0;
            this.t_project.ShortcutsEnabled = true;
            this.t_project.Size = new System.Drawing.Size(129, 23);
            this.t_project.TabIndex = 9;
            this.t_project.UseSelectable = true;
            this.t_project.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.t_project.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 640);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 13);
            this.label15.TabIndex = 33;
            this.label15.Text = "Środowisko:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bSave);
            this.groupBox2.Controls.Add(this.bLoad);
            this.groupBox2.Controls.Add(this.bClear);
            this.groupBox2.Location = new System.Drawing.Point(6, 536);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(238, 52);
            this.groupBox2.TabIndex = 32;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Opcje";
            // 
            // bSave
            // 
            this.bSave.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.bSave.Location = new System.Drawing.Point(162, 19);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(70, 23);
            this.bSave.TabIndex = 10;
            this.bSave.Text = "Zapisz";
            this.bSave.UseSelectable = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // bLoad
            // 
            this.bLoad.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.bLoad.Location = new System.Drawing.Point(6, 19);
            this.bLoad.Name = "bLoad";
            this.bLoad.Size = new System.Drawing.Size(70, 23);
            this.bLoad.TabIndex = 8;
            this.bLoad.Text = "Wczytaj";
            this.bLoad.UseSelectable = true;
            this.bLoad.Click += new System.EventHandler(this.bLoad_Click);
            // 
            // bClear
            // 
            this.bClear.FontWeight = MetroFramework.MetroButtonWeight.Regular;
            this.bClear.Location = new System.Drawing.Point(82, 19);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(74, 23);
            this.bClear.TabIndex = 9;
            this.bClear.Text = "Wyczyść";
            this.bClear.UseSelectable = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 678);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Szablon:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 491);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(123, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Numer dokumentu obcy:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 416);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Podtytuł:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 318);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Tytuł:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(76, 294);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Data:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(66, 262);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Nazwa:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 233);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Zamawiający:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Konstruktor:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(51, 175);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Projektant:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(68, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Obiekt:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(76, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Faza:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Rewizja:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Numer dokumentu:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numer projektu:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(192, 13);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(129, 13);
            this.label16.TabIndex = 7;
            this.label16.Text = "Nazwa folderu z raportami";
            // 
            // checkedLists
            // 
            this.checkedLists.FormattingEnabled = true;
            this.checkedLists.Location = new System.Drawing.Point(3, 12);
            this.checkedLists.Name = "checkedLists";
            this.checkedLists.Size = new System.Drawing.Size(183, 229);
            this.checkedLists.TabIndex = 1;
            this.checkedLists.SelectedIndexChanged += new System.EventHandler(this.checkedLists_SelectedIndexChanged);
            // 
            // bBOM
            // 
            this.bBOM.Location = new System.Drawing.Point(192, 61);
            this.bBOM.Name = "bBOM";
            this.bBOM.Size = new System.Drawing.Size(219, 37);
            this.bBOM.TabIndex = 12;
            this.bBOM.Text = "Wykonaj zestawienie";
            this.bBOM.UseSelectable = true;
            this.bBOM.Click += new System.EventHandler(this.bBOM_Click);
            // 
            // _folder_output
            // 
            // 
            // 
            // 
            this._folder_output.CustomButton.Image = null;
            this._folder_output.CustomButton.Location = new System.Drawing.Point(197, 1);
            this._folder_output.CustomButton.Name = "";
            this._folder_output.CustomButton.Size = new System.Drawing.Size(21, 21);
            this._folder_output.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this._folder_output.CustomButton.TabIndex = 1;
            this._folder_output.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this._folder_output.CustomButton.UseSelectable = true;
            this._folder_output.CustomButton.Visible = false;
            this._folder_output.Lines = new string[0];
            this._folder_output.Location = new System.Drawing.Point(192, 32);
            this._folder_output.MaxLength = 32767;
            this._folder_output.Name = "_folder_output";
            this._folder_output.PasswordChar = '\0';
            this._folder_output.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this._folder_output.SelectedText = "";
            this._folder_output.SelectionLength = 0;
            this._folder_output.SelectionStart = 0;
            this._folder_output.ShortcutsEnabled = true;
            this._folder_output.Size = new System.Drawing.Size(219, 23);
            this._folder_output.TabIndex = 42;
            this._folder_output.UseSelectable = true;
            this._folder_output.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this._folder_output.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tabCtrl
            // 
            this.tabCtrl.Controls.Add(this.tabMain);
            this.tabCtrl.Controls.Add(this.tabVE);
            this.tabCtrl.Controls.Add(this.tabST);
            this.tabCtrl.Controls.Add(this.tabM);
            this.tabCtrl.Controls.Add(this.tabS);
            this.tabCtrl.Controls.Add(this.tabSMSM);
            this.tabCtrl.Controls.Add(this.tabSMSW);
            this.tabCtrl.Controls.Add(this.tabA);
            this.tabCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrl.Location = new System.Drawing.Point(270, 60);
            this.tabCtrl.Name = "tabCtrl";
            this.tabCtrl.SelectedIndex = 0;
            this.tabCtrl.Size = new System.Drawing.Size(985, 760);
            this.tabCtrl.TabIndex = 43;
            this.tabCtrl.UseSelectable = true;
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.pictureBox1);
            this.tabMain.Controls.Add(this._folder_output);
            this.tabMain.Controls.Add(this.bBOM);
            this.tabMain.Controls.Add(this.label16);
            this.tabMain.Controls.Add(this.checkedLists);
            this.tabMain.HorizontalScrollbarBarColor = true;
            this.tabMain.HorizontalScrollbarHighlightOnWheel = false;
            this.tabMain.HorizontalScrollbarSize = 10;
            this.tabMain.Location = new System.Drawing.Point(4, 38);
            this.tabMain.Name = "tabMain";
            this.tabMain.Size = new System.Drawing.Size(977, 718);
            this.tabMain.TabIndex = 0;
            this.tabMain.Text = "Raporty";
            this.tabMain.VerticalScrollbarBarColor = true;
            this.tabMain.VerticalScrollbarHighlightOnWheel = false;
            this.tabMain.VerticalScrollbarSize = 10;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(794, 535);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(180, 180);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // tabVE
            // 
            this.tabVE.Controls.Add(this.gridVE);
            this.tabVE.Location = new System.Drawing.Point(4, 38);
            this.tabVE.Name = "tabVE";
            this.tabVE.Size = new System.Drawing.Size(977, 718);
            this.tabVE.TabIndex = 7;
            this.tabVE.Text = "VE";
            // 
            // gridVE
            // 
            this.gridVE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridVE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridVE.Location = new System.Drawing.Point(0, 0);
            this.gridVE.Name = "gridVE";
            this.gridVE.Size = new System.Drawing.Size(977, 718);
            this.gridVE.TabIndex = 45;
            // 
            // tabST
            // 
            this.tabST.Controls.Add(this.gridST);
            this.tabST.Location = new System.Drawing.Point(4, 38);
            this.tabST.Name = "tabST";
            this.tabST.Size = new System.Drawing.Size(977, 718);
            this.tabST.TabIndex = 8;
            this.tabST.Text = "ST";
            // 
            // gridST
            // 
            this.gridST.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridST.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridST.Location = new System.Drawing.Point(0, 0);
            this.gridST.Name = "gridST";
            this.gridST.Size = new System.Drawing.Size(977, 718);
            this.gridST.TabIndex = 46;
            // 
            // tabM
            // 
            this.tabM.Controls.Add(this.gridM);
            this.tabM.Location = new System.Drawing.Point(4, 38);
            this.tabM.Name = "tabM";
            this.tabM.Size = new System.Drawing.Size(977, 718);
            this.tabM.TabIndex = 3;
            this.tabM.Text = "M";
            // 
            // gridM
            // 
            this.gridM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridM.Location = new System.Drawing.Point(0, 0);
            this.gridM.Name = "gridM";
            this.gridM.Size = new System.Drawing.Size(977, 718);
            this.gridM.TabIndex = 2;
            // 
            // tabS
            // 
            this.tabS.Controls.Add(this.gridS);
            this.tabS.Location = new System.Drawing.Point(4, 38);
            this.tabS.Name = "tabS";
            this.tabS.Size = new System.Drawing.Size(977, 718);
            this.tabS.TabIndex = 9;
            this.tabS.Text = "S";
            // 
            // gridS
            // 
            this.gridS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridS.Location = new System.Drawing.Point(0, 0);
            this.gridS.Name = "gridS";
            this.gridS.Size = new System.Drawing.Size(977, 718);
            this.gridS.TabIndex = 47;
            // 
            // tabSMSM
            // 
            this.tabSMSM.Controls.Add(this.gridSMSM);
            this.tabSMSM.Location = new System.Drawing.Point(4, 38);
            this.tabSMSM.Name = "tabSMSM";
            this.tabSMSM.Size = new System.Drawing.Size(977, 718);
            this.tabSMSM.TabIndex = 4;
            this.tabSMSM.Text = "SMS_M";
            // 
            // gridSMSM
            // 
            this.gridSMSM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSMSM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSMSM.Location = new System.Drawing.Point(0, 0);
            this.gridSMSM.Name = "gridSMSM";
            this.gridSMSM.Size = new System.Drawing.Size(977, 718);
            this.gridSMSM.TabIndex = 2;
            // 
            // tabSMSW
            // 
            this.tabSMSW.Controls.Add(this.gridSMSW);
            this.tabSMSW.Location = new System.Drawing.Point(4, 38);
            this.tabSMSW.Name = "tabSMSW";
            this.tabSMSW.Size = new System.Drawing.Size(977, 718);
            this.tabSMSW.TabIndex = 5;
            this.tabSMSW.Text = "SMS_W";
            // 
            // gridSMSW
            // 
            this.gridSMSW.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSMSW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSMSW.Location = new System.Drawing.Point(0, 0);
            this.gridSMSW.Name = "gridSMSW";
            this.gridSMSW.Size = new System.Drawing.Size(977, 718);
            this.gridSMSW.TabIndex = 2;
            // 
            // tabA
            // 
            this.tabA.Controls.Add(this.gridA);
            this.tabA.Location = new System.Drawing.Point(4, 38);
            this.tabA.Name = "tabA";
            this.tabA.Size = new System.Drawing.Size(977, 718);
            this.tabA.TabIndex = 6;
            this.tabA.Text = "A";
            // 
            // gridA
            // 
            this.gridA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridA.Location = new System.Drawing.Point(0, 0);
            this.gridA.Name = "gridA";
            this.gridA.Size = new System.Drawing.Size(977, 718);
            this.gridA.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1275, 840);
            this.Controls.Add(this.tabCtrl);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1275, 840);
            this.Name = "MainForm";
            this.Text = "Reports 2";
            this.Resize += new System.EventHandler(this.mainForm_Resize);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tabCtrl.ResumeLayout(false);
            this.tabMain.ResumeLayout(false);
            this.tabMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabVE.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridVE)).EndInit();
            this.tabST.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridST)).EndInit();
            this.tabM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridM)).EndInit();
            this.tabS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridS)).EndInit();
            this.tabSMSM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSMSM)).EndInit();
            this.tabSMSW.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSMSW)).EndInit();
            this.tabA.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridA)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox checkedLists;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private MetroFramework.Controls.MetroButton b_rev;
        private MetroFramework.Controls.MetroTextBox t_phase;
        private MetroFramework.Controls.MetroTextBox t_document;
        private MetroFramework.Controls.MetroTextBox t_project;
        private MetroFramework.Controls.MetroComboBox c_rev;
        private MetroFramework.Controls.MetroDateTime d_date;
        private MetroFramework.Controls.MetroTextBox t_name;
        private MetroFramework.Controls.MetroTextBox t_contractor;
        private MetroFramework.Controls.MetroTextBox t_checkedby;
        private MetroFramework.Controls.MetroTextBox t_createdby;
        private MetroFramework.Controls.MetroTextBox t_object;
        private MetroFramework.Controls.MetroTextBox t_document_other;
        private MetroFramework.Controls.MetroTextBox t_subtitle;
        private MetroFramework.Controls.MetroTextBox t_title;
        private MetroFramework.Controls.MetroButton bSave;
        private MetroFramework.Controls.MetroButton bLoad;
        private MetroFramework.Controls.MetroButton bClear;
        private MetroFramework.Controls.MetroButton _loadreports;
        private MetroFramework.Controls.MetroComboBox c_environment;
        private MetroFramework.Controls.MetroComboBox c_template;
        private MetroFramework.Controls.MetroButton bOpen;
        private MetroFramework.Controls.MetroButton bExcel;
        private MetroFramework.Controls.MetroButton bBOM;
        private MetroFramework.Controls.MetroTextBox _folder_output;
        private MetroFramework.Controls.MetroTabControl tabCtrl;
        private MetroFramework.Controls.MetroTabPage tabMain;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tabM;
        private System.Windows.Forms.DataGridView gridM;
        private System.Windows.Forms.TabPage tabSMSM;
        private System.Windows.Forms.DataGridView gridSMSM;
        private System.Windows.Forms.TabPage tabSMSW;
        private System.Windows.Forms.DataGridView gridSMSW;
        private System.Windows.Forms.TabPage tabA;
        private System.Windows.Forms.DataGridView gridA;
        private System.Windows.Forms.TabPage tabVE;
        private System.Windows.Forms.DataGridView gridVE;
        private System.Windows.Forms.TabPage tabST;
        private System.Windows.Forms.DataGridView gridST;
        private System.Windows.Forms.TabPage tabS;
        private System.Windows.Forms.DataGridView gridS;
    }
}

