﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Reports2
{
    public partial class Revisions : Form
    {
        string _file = "";

        public Revisions(string file_revisions)
        {
            InitializeComponent();
            _file = file_revisions;
            _revisions.Text = File.ReadAllText(_file);
            _revisions.SelectionStart = _revisions.Text.Length;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            File.WriteAllText(_file, _revisions.Text);
            DialogResult = DialogResult.OK;
        }
    }
}
